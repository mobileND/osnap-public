// Dependencies
var application_root = __dirname,
	express = require('express'),
	path = require('path'),
	cookieParser = require('cookie-parser'),
	methodOverride = require('method-override'),
	bodyParser = require('body-parser'),
	http = require('http'),
	crontab = require('node-crontab'),
	fs = require('fs'),
	util = require('util');

//Redirect stdout logs to file
var log_file = fs.createWriteStream(__dirname + '/info.log', {flags : 'w'});
var err_file = fs.createWriteStream(__dirname + '/err.log', {flags: 'w'});

console.log = function(d) {
	log_file.write(util.format(d) + '\n');
}

console.log_err = function(d) {
	log_file.write(util.format(d) + '\n');
	err_file.write(util.format(d) + '\n');
}

// Create Server
var app = express();

// Configure Server
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride());
app.use(cookieParser());
app.use(app.router);
app.use(express.static(path.join(application_root, 'site')));
app.use(express.errorHandler({dumpExceptions: true, showStack: true}));
app.set('port', process.env.PORT || 8080);
app.set('email', process.env.EMAIL || "");
app.set('emailPass', process.env.EMAIL_PASS || "");
app.set('IPString', process.env.IPString);

//Check if valid IP address for public API
//The Accepted IPs are stored in a string
//That is an environment variable
app.checkIP = function(request){
	var ip = request.header('x-forwarded-for');	
	var ipString = app.get('IPString');

	if(ipString.search(ip) != -1){
		return true;
	} else {
		return false;
	}	
}

var server = require('http').Server(app);
var io = require('socket.io')(server);

//Connect routes
require("./routes")(app, io);

// Start Server
var successString = 'OSNAP!!!! The server is listening on port ' + app.get('port') + ' in ' + app.settings.env + ' mode.';
server.listen(app.get('port'), function(){
	console.log(successString);
});

/******Node Cron Job to shut down OSNAP at a certain time******/
//This script will be used as a cron job to turn off OSNAP
//at the requested time. The "type" variable exists because
//this is using the admin API, which ony those with admin
//cookies can access. To get around that we use the "crobJob" type

var jobId = crontab.scheduleJob("0 3 * * *", function(){
	var options = {
		host: 'osnap.nd.edu',
		port: '80',
		path: '/api/admin/admindata',
		method: 'PUT',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'
		}
	};

	var req = http.request(options, function(res) {
		console.log("Node script to turn off OSNAP has run and ended with HTTP status code: " + res.statusCode);
	});

	// write the request parameters
	req.write('type=cronJob&serviceStatus=0');
	req.end();
});
