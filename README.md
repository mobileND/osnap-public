# README #

### What is this repository for? ###

* This web app is the management system for processing rides for a student safe ride system
* The rides come in real time and then they can be assigned to different drivers and statistics/logs can be generated
* This is a web app built using node.js, socket.io, and mongodb which 
* Version 1.0.0

### Dependencies ###
* All of the npm modules needed are listed in package.json

### Configuration ###
* Download the repo
* Run 'npm install'
* Create a server environment variables file with these variables:

    * export NODE_ENV=production
    * export PORT=8080
    * export EMAIL="youremailer@domain.com"
    * export EMAIL_PASS="emailPassword"
    * export IPString="0.0.0.0;1.1.1.1"
        * IPString consists of IPs allowed to send requests to the server. They are separated by ';'. This is used to 
protect the API calls, but can be disabled if need be

* Change the database name in database.js to whatever you'd like it to be
* Change the hostname in site/js/config.js to the appropriate hostname 

### Notes ###
* Does not work in IE and therefore IE has been blocked
* When you create the first account on the site you must manually change the permission in the db
to make that user an admin because users are registered automatically as drivers
* Development was suspended on the Driver View of the app so it is not up to date
* The web app can be used via desktop or iPad - you can save the app as an icon on your iPad home screen

### Who do I talk to? ###

* contact mobileND for support
* http://mobile.nd.edu/contact/