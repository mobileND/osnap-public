var database = require('../../database');
var UserModel = database.UserModel;
var nodemailer = require('nodemailer');
var randomstring = require('randomstring');
//User Functions
////////////////////////////////


function newUser(request, response){
	var newUser = new UserModel({
		username: request.body.username,
		password: request.body.password,
		email: request.body.email,
		isAdmin: false,
		isDriver: true //default to driver status
	});
	
	return UserModel.findOne({username: request.body.username}, function(err, user){
		if(user != null){
			console.log("Error: username already taken");
			return response.send({status: 403, message: "Username already taken"});
		}
		else{
			return UserModel.findOne({email: request.body.email}, function(err, user2){
				if(user2 != null){
					return response.send({status: 403, message: "Account already associated with this email"});
				}
				else{
					return newUser.save(function(err){
						if(!err){
							console.log('New user created');
							return response.send(newUser);
						} else{
							console.log_err(err);
						}
					});								
				}
			});		
		}			
	});
}

function authenticate(request, response){
	return UserModel.findOne({ username: request.body.username}, function(err, user){
		if (err || user == null){
			console.log_err("Username does not exist");	
			return response.send({status: 404, message: "Username does not exist"});			
		} 
		
		user.comparePassword(request.body.password, function(err, isMatch){
			if (err){
				console.log_err("Error attempting login");
				return response.send({ status: 500, message: "Error processing login request"});
			} 
			
			if (!isMatch){
				console.log("Invalid login attempt");
				return response.send({ status: 401, message: "Unauthorized"});							
			}
			console.log("Login attempt successful");
			return response.send(user);
		});
	});
}

function changePassword(request, response, cookie){
	return UserModel.findOne({ username: cookie.name}, function(err, user){
		if (err || user == null){
			console.log_err("Username does not exist");	
			return response.send({status: 404, message: "Username does not exist"});			
		} 
		
		user.comparePassword(request.body.password, function(err, isMatch){
			if (err){
				console.log_err("Error attempting password validation");
				return response.send({ status: 500, message: "Error processing login request"});
			} 
			
			if (!isMatch){
				console.log("Invalid password submitted");
				return response.send({ status: 401, message: "Unauthorized"});							
			} else{					
				user.password = request.body.newPassword;
				return user.save(function(err){
					if(!err){
						console.log('User ' + user.username + "'s password has been updated.");
						return response.send({status: 200, message: "OK"});			
					} else{
						console.log_err(err);
					}
				});							
			}
		});
	});
}

function updateUser(request, response){
	return UserModel.findOne({'username': request.params.username}, function(err, user){
		
		if(request.body.email != null && request.body.email !== undefined){
			//We do this because if it is an email update request it cannot 
			//simultaneously be a flags update request
			if(request.body.email != user.email){
				user.email = request.body.email;
				
				return user.save(function(err){
					if(!err){
						console.log('User ' + request.params.username + "'s email has been updated.");
						return response.send({status: 200});		
					} else{
						console.log_err(err);
						return response.send({status: 500});
					}
				});						
			} else{
				return response.send({status: 200, message: 'Same password.'});
			}
		} else{
			if(request.body.isAdmin != null && request.body.isAdmin !== undefined){
				user.isAdmin = (request.body.isAdmin.toString() == "true");
			}
			if(request.body.isDriver != null && request.body.isDriver !== undefined){
				user.isDriver = (request.body.isDriver.toString() == "true");
			}			
		
			return user.save(function(err){
				if(!err){
					console.log('User ' + request.params.username + ' updated.');
					return response.send(200);
				} else{
					console.log_err(err);
					return response(500);
				}
			});				
		}
	});
}	

function getUsers(request, response){
	return UserModel.find(function(err, users){
		for(var i = 0; i < users.length; i++){
			users[i].toJSON();
		}			
		if(!err){
			return response.send(users);
		} else{
			return console.log_err(err);
		}
	});		
}

function returnUserEmail(request, response, username){
	return UserModel.findOne({'username': username}, function(err, user){	
		return response.send({status: 200, email: user.email});				
	});
}

function resetUserInfo(request, response, email, password){
	return UserModel.findOne({'email': request.body.email}, function(err, user){
		if(!err){
			
			if(user == null){
				//This means they entered an invalid email address
				return response.send({status: 401});
			}
			
			var newPassword = randomstring.generate(10);
			user.password = newPassword;
			return user.save(function(err){
				if(!err){
					console.log('User ' + user.username + "'s password has been reset to a random password.");
					//Until we integrate Shibboleth into the app we use the app login screen
					//This is the password to the emailer acct for if someone forgets their password
					var transporter = nodemailer.createTransport({
						service: 'Gmail',
						auth: {
							user: email,
							pass: password
						}
					});

					var mailOptions = {
						from: 'DO NOT REPLY <osnapemailer@gmail.com>',
						to: user.email,
						subject: 'OSNAP Login Information',
						text: 'Your password has been reset. Your login information is as follows: username: ' + user.username + ', password: ' + newPassword + '.'
					};

					transporter.sendMail(mailOptions, function(error, info){
				    		if(error){
								console.log(err);
								return response.send({status: 500, message: "Our server has encountered an error trying to email you. Please try again!"});
				    		}else{
				        		console.log('Password reset message sent: ' + info.response);
								return response.send({status: 200});
				    		}
					});						
				} else{
					console.log_err(err);
					return response.send({status: 500, message: "Yikes, we've encountered an Internal Server Error! Please try again!"});
				}
			});					
		} else{
			return response.send({status: 500, message: "Yikes, we've encountered an Internal Server Error! Please try again!"});
		}
	});
}

exports.newUser = newUser;
exports.updateUser = updateUser;
exports.authenticate = authenticate;
exports.changePassword = changePassword;
exports.getUsers = getUsers;
exports.returnUserEmail = returnUserEmail;
exports.resetUserInfo = resetUserInfo;
