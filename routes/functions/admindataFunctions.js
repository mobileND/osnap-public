var database = require('../../database');
var AdminDataModel = database.AdminDataModel;
var PlaceModel = database.PlaceModel;

function returnAdminData(request, response){
	return AdminDataModel.find(function(err, admindata){
		if(!err){
			if(admindata.length == 0 ){
				var data = new AdminDataModel({
					maxRiders: 4,
					serviceStatus: 1
				});
		
				return data.save(function(err){
					if(!err){
						console.log('New AdminData field created');
						return response.send(data);
					} else{
						console.log(err);
					}
				});					
				
			}
			else{
				admindata[0].locations.sort(
					function (a,b) {
						  if (a.name < b.name)
						     return -1;
						  if (a.name > b.name)
						    return 1;
						  return 0;
					}
				);	
				return response.send(admindata);					
			}
		} else{
			return console.log_err(err);
		}
	});		
};

function addAdminPlaces(request, response){
	if(request.body.name != "" && request.body.name != null){
		var place = new PlaceModel({
			name: request.body.name,
			disabled: (request.body.disabled.toString() === "true"),
			description: request.body.description,
			shortName: request.body.shortName
		});
		
		return AdminDataModel.findOne(function(err, admindata){
			admindata.locations.push(place);
		
			return admindata.save(function(err){
				if(!err){
					console.log('Admin data places updated');											
					return response.send(200);
				} else{
					console.log_err(err);
					return response.send(500);
				}
			});
		});			
	}
	else{
		console.log("Error: cannot create new location because no name specified.")
	}		
}

function updateAdminPlaces(request, response, io){
	if(request.body.maxRiders != null && request.body.maxRiders != ""){
		return AdminDataModel.findOne(function(err, admindata){
			admindata.maxRiders = parseInt(request.body.maxRiders);
			
			return admindata.save(function(err){
				if(!err){
					console.log('Successfully changed the value of maxRiders to ' + admindata.maxRiders);
					//The status codes are done to control the action taken by the client
					//The actions are displaying a success message or an error message
					return response.send(200);
				}else{
					console.log_err(err);
					return response.send(500);
				}
			});
		});
	}
    else if(request.body.serviceStatus != null && request.body.serviceStatus != ""){
            return AdminDataModel.findOne(function(err, admindata){
                    admindata.serviceStatus = parseInt(request.body.serviceStatus);

                    return admindata.save(function(err){
                            if(!err){
                                    console.log('Successfully changed the value of serviceStatus to ' + admindata.serviceStatus);
                                    //The status codes are done to control the action taken by the client
                                    //The actions are displaying a success message or an error message
									if(request.body.type == "cronJob"){
										io.emit('cron-job', 'cron-job');	
									} else {
										io.emit('servicestatus-change', 'servicestatus-change');
									}
									
                                    return response.send(200);
                            }else{
                                    console.log_err(err);
                                    return response.send(500);
                            }
                    });
            });
    }
	else{
		if(request.body.name != "" && request.body.name != null ){
			
			var newVal = null; 
			if(request.body.disabled !== undefined) newVal = (request.body.disabled.toString() === "true");
			var description = request.body.description;
			var shortName = request.body.shortName;
			
			return AdminDataModel.findOne(function(err, admindata){
				for(var i = 0; i < admindata.locations.length; i++){
					if(admindata.locations[i].name == request.body.name){
						if(newVal != null) admindata.locations[i].disabled = newVal;
						if(description !== undefined) admindata.locations[i].description = description;
						if(shortName !== undefined) admindata.locations[i].shortName = shortName;
						i = admindata.locations.length;
					}
				}
		
				return admindata.save(function(err){
					if(!err){
						console.log('Location ' + request.body.name + ' has been uppdated');								
						return response.send(200);
					} else{
						console.log_err(err);
						return response.send(500);
					}
				});
			});					
		}
	}		
}

function deleteAdminPlaces(request, response){
	if(request.body.name != null && request.body.name != ""){
		return AdminDataModel.findOne(function(err, admindata){
			var array = new Array();
			for(var i = 0; i < admindata.locations.length; i++){
				if(admindata.locations[i].name != request.body.name){
					array.push(admindata.locations[i]);
				}
			}
			admindata.locations = array;
	
			return admindata.save(function(err){
				if(!err){
					console.log('Location ' + request.body.name + ' has been deleted');
					return response.send(200);
				} else{
					console.log_err(err);
					return response.send(500);
				}
			});
		});		
	}		
}

exports.returnAdminData = returnAdminData;
exports.addAdminPlaces = addAdminPlaces;
exports.updateAdminPlaces = updateAdminPlaces;
exports.deleteAdminPlaces = deleteAdminPlaces;

