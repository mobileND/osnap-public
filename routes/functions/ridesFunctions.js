var database = require('../../database');
var RideRequestModel = database.RideRequestModel;
var moment = require('moment-timezone');

//Ride Request Functions
function returnRides(request, response, flag){
	if(flag == "all"){
		return RideRequestModel.find(function(err, rides){
			if(!err){
				return response.send(rides);
			} else{
				return console.log_err(err);
			}
		});				
	} else {
		//return the active rides
		return RideRequestModel.find({ $and: [{ status: {'$ne': 4}} ,{ status: {'$ne': 5}}, { status: {'$ne': 6}}] }, function(err, rides){
			if(!err){
				return response.send({active: rides.length});
			} else{
				return console.log_err(err);
			}
		});
	}
}

function returnSingleRide(request, response){
	return RideRequestModel.findById(request.params.id, function(err, ride){
		if(!err){
			return response.send(ride);
		} else{
			console.log_err(err);
		}
	});		
}

function insertNewRide(request, response, io){

	var time = moment().tz('America/Detroit');
	var date = time;

	date = time.format('M/DD/YY');
	time = time.format('h:mm:ss A');
	
	var message = "Request generated: " + date + ", " + time;
	var array = new Array();
	array.push(message);	
	var ride = new RideRequestModel({
		location: request.body.location,
		destination: request.body.destination,
		name: request.body.name,
		numberPassengers: request.body.passengers,
		phone: request.body.phone,
		closed: false,
		cancelled: false,
		status: 0,
		time: time,
		date: date,
		assignedTo: "",
		log: array
	});
		
	return ride.save(function(err){
		if(!err){
			console.log('New ride created');
			io.emit('refresh', 'new ride added');
			return response.send(ride);
		} else{
			console.log_err(err);
		}
	});		
}

function updateRide(request, response, io){
	return RideRequestModel.findById(request.params.id, function(err, ride){				
	
		//Generate Time Stamp
		//Used Detroit because trying to set timezone to EST 	
    	var time = moment().tz('America/Detroit');
    	var date = time;

    	date = time.format('M/DD/YY');
    	time = time.format('h:mm:ss A');

		var array = new Array();					
		
		//Check for ride cancel 1st
		if( request.body.cancel !== undefined && request.body.cancel == "true"){
			ride.status = 6;						
			var message = 'cancelled;' + 'Ride has been cancelled: ' + date + ", " + time;	

			//Add message to log	
			if(ride.log != []){
				array = ride.log;
				array.push(message);
			}
			else array.push(message);						
			ride.log = array;									
			
			//If it is a ride cancel we don't have to worry about anything else
			//We can just return
			return ride.save(function(err){
				if(!err){
					console.log('Ride ' + request.params.id + ' cancelled.');
					io.emit('ride-update', 'ride updated');							
					return response.send(ride);
				} else{
					console.log_err(err);
				}
			});	

		}
		else{			
			if( request.body.cancelled !== undefined && request.body.cancelled == true){
				ride.cancelled = true;
				console.log("Successfully confirmed cancellation of ride");
				var message = 'cancelled;' + 'Ride cancellation has successfully been confirmed: ' + date + ", " + time;
				
				//Add message to log
				if(ride.log != []){
					array = ride.log;
					array.push(message);
				}
				else array.push(message);					
				
			} else{
				//This is done to fix the problem of the cancelled attribute of rides
				//remaining true even after their status is changed to something else
				if(ride.cancelled == true) ride.cancelled = false;
			}
			
			//Check which attributes of the object are being updated
			if( request.body.status !== undefined && request.body.status != null && request.body.status != ride.status){
				var oldVal = ride.status;
				var newVal = request.body.status;
				ride.status = request.body.status;
				console.log("Successfully updated status");
				var message = 'status;' + oldVal + ';' + newVal + ';' + date + ", " + time;
			
				//Add message to log
				if(ride.log != []){
					array = ride.log;
					array.push(message);
				}
				else array.push(message);
			}	
			//ride.status = request.body.status || ride.status;
			if ( request.body.closed != null && request.body.closed !== undefined && request.body.closed != ride.closed){
				var oldVal = ride.closed;
				var newVal = (request.body.closed.toString() === "true");
				ride.closed = (request.body.closed.toString() === "true");
				console.log("Successfully updated closed value");
				var message = 'closed;' + oldVal + ';' + newVal + ';' + date + ", " + time;	
			
				//Add message to log	
				if(ride.log != []){
					array = ride.log;
					array.push(message);
				}
				else array.push(message);						
			}
			
			if ( request.body.location != null && request.body.location !== undefined && request.body.location != ride.location){
				var oldVal = ride.location;
				var newVal = request.body.location;
				ride.location = newVal;
				console.log("Successfully updated location value");
				var message = 'location;' + oldVal + ';' + newVal + ';' + date + ", " + time;
	
				//Add message to log	
				if(ride.log != []){
					array = ride.log;
					array.push(message);
				}
				else array.push(message);										
			}
			
			if ( request.body.destination != null && request.body.destination !== undefined && request.body.destination != ride.destination){
				var oldVal = ride.destination;
				var newVal = request.body.destination;
				ride.destination = newVal;
				console.log("Successfully updated destination value");
				var message = 'destination;' + oldVal + ';' + newVal + ';' + date + ", " + time;
	
				//Add message to log	
				if(ride.log != []){
					array = ride.log;
					array.push(message);
				}
				else array.push(message);										
			}			
			
			if ( request.body.numberPassengers != null && request.body.numberPassengers !== undefined && request.body.numberPassengers != ride.numberPassengers){
				var oldVal = ride.numberPassengers;
				var newVal = request.body.numberPassengers;
				ride.numberPassengers = newVal;
				console.log("Succesfully updated amount of passengers");
				var message = 'passengers;' + oldVal + ';' + newVal + ';' + date + ", " + time;
				
				//Add message to log	
				if(ride.log != []){
					array = ride.log;
					array.push(message);
				}
				else array.push(message);						
			}	
		
			//If user is assigned then handle that and then return
			//Else save ride and return 
			if( typeof request.body.assignedTo !== undefined && request.body.assignedTo != null && request.body.assignedTo != ride.assignedTo){
				ride.assignToDriver(request.body.assignedTo, function(err, success){
					if (err){
						console.log("Error assigning user");
					}
				
					if (success){
						var oldVal = ride.assignedTo;
						var newVal = request.body.assignedTo;
						if(newVal == ""){
							console.log("Driver removed from ride " + request.body.id + " no replacement driver assigned.")
						} else {
							console.log("Successfully assigned driver " + newVal + " to ride " + request.body.id);									
						}

						ride.assignedTo = newVal;	
						var message = 'driver;' + oldVal + ';' + newVal + ';' + date + ", " + time;	

						//Add message to log
						if(ride.log != []){
							array = ride.log;
							array.push(message);
						}
						else array.push(message);
						ride.log = array;											
					}
					else console.log("Specified driver does not exist");
				
					return ride.save(function(err){
						//This is used to trigger the success & error callbacks for model.save on backbone client
						//Because a JSON object must be returned a JSON object is returned on success
						//However, even a 400 error code in the status attribute was triggering a success callback
						//So I figured out that doing response.send(400) will trigger the error callback
						if(!err){
							console.log('Ride ' + request.params.id + ' updated.');
							io.emit('ride-update', 'ride updated');							
							return response.json({"success" : "Updated Successfully", "status" : 200});
						} else{
							console.log_err(err);
							return response.send(400); 								
						}
					});					
				});
			}
			else
			{
				ride.log = array;
				return ride.save(function(err){
					//Same explanation as aforementioned comment
					if(!err){
						console.log('Ride ' + request.params.id + ' updated.');
						io.emit('ride-update', 'ride updated');							
						return response.json({"success" : "Updated Successfully", "status" : 200});
					} else{
						console.log_err(err);
						return response.send(400); 						
					}
				});				
			}				
		}			
		
	});		
}

function deleteRide(request, response, io){
        return RideRequestModel.remove( { _id: request.params.id}, function(err, removed){
                if(!err){
			if(removed == 1) {
                                io.emit('ride-update', 'ride updated');
                        	return response.send(200);
			} else {
				return response.send(404);
			}
                } else{
                        console.log_err(err);
			return response.send(500);
                }
	});
}

exports.returnRides = returnRides;
exports.returnSingleRide = returnSingleRide;
exports.insertNewRide = insertNewRide;
exports.updateRide = updateRide;
exports.deleteRide = deleteRide;
