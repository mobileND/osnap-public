var userRouteFunctions = require('./functions/usersFunctions');

module.exports = function(app){
	//User Routes
	////////////////////////////////

	//Insert a new user - everyone should be able to use this (not sure how to prevent abuse though)
	app.post('/api/users', function(request, response){
		userRouteFunctions.newUser(request, response);			
	});

	//Get list of all users - Admin
	app.get('/api/admin/users', function(request,response){
		var cookie = null;
	
		if(request.cookies['user'] !== undefined){
			cookie = JSON.parse(request.cookies['user']);	
		}
		
		if(cookie != null && cookie.view == 0){
			return userRouteFunctions.getUsers(request, response);
		} else{
			response.status(404);
			return response.send("Cannot GET " + request.url);
		}
	});

	//Authenticate user - Everyone should be able to use this
	//Only in use until Shibboleth integration
	app.post('/api/users/authenticate', function(request, response){
		userRouteFunctions.authenticate(request, response);
	});

	//Return user email - only for Admins/Drivers
	//Only in use until Shibboleth integration
	app.get('/api/users/password', function(request, response){	
		var cookie = null;
	
		if(request.cookies['user'] !== undefined){
			cookie = JSON.parse(request.cookies['user']);	
		}
		
		if(cookie != null && (cookie.view == 0 || cookie.view == 1)){
			return userRouteFunctions.returnUserEmail(request, response, cookie.name);
		} else{
			response.status(404);
			return response.send("Cannot GET " + request.url);
		}	
	});		

	//Change Password - Set up so only Admins/Drivers can use
	//Only in use until Shibboleth integration
	app.post('/api/users/password', function(request, response){

		var cookie = null;
	
		if(request.cookies['user'] !== undefined){
			cookie = JSON.parse(request.cookies['user']);	
		}
		else{
			return response.send("Cannot POST " + request.url);
		}
	
		userRouteFunctions.changePassword(request, response, cookie);
	});	

	//Update a specific user's flags - Admin
	app.put('/api/admin/users/:username', function(request, response){			
		var cookie = null;
	
		if(request.cookies['user'] !== undefined){
			cookie = JSON.parse(request.cookies['user']);	
		}
	
		if(request.body.email != null && request.body.email !== undefined){
			if(cookie != null && (cookie.view == 0 || cookie.view == 1)){
				return userRouteFunctions.updateUser(request, response);
			} else{
				response.status(404);
				return response.send("Cannot PUT " + request.url);
			}				
		} else{
			if(cookie != null && cookie.view == 0){
				return userRouteFunctions.updateUser(request, response);
			} else{
				response.status(404);
				return response.send("Cannot PUT " + request.url);
			}			
		}
			
	});

	//Email user info if they forgot it - Drivers/Admins
	//Everyone should be able to use this because it'll be on login page
	//Only in use until Shibboleth integration
	app.post('/api/users/reset', function(request, response){
		return userRouteFunctions.resetUserInfo(request, response, app.get('email'), app.get('emailPass'));	
	});
	
}
