var adminDataRouteFunctions = require('./functions/admindataFunctions');

module.exports = function (app, io) {
	
	//Admin Data Routes
	//////////////////////////////////
	
	//Get admin settings - public
	app.get('/api/admindata', function(request,response){
		if(app.checkIP(request)){
			adminDataRouteFunctions.returnAdminData(request, response);
		} else{
			response.status(404);
			return response.send("Cannot GET " + request.url);
		}	
	});

	//Get admin settings - admin
	app.get('/api/admin/admindata', function(request,response){
		var cookie = null;
	
		if(request.cookies['user'] !== undefined){
			cookie = JSON.parse(request.cookies['user']);	
		}
	
		//For the GET call only both drivers and admins can 
		//access this. This is done so that drivers can 
		//also use the admindatac class, which use the admin API
		//This makes it so that an identical class using the public API
		if(cookie != null){
			return adminDataRouteFunctions.returnAdminData(request, response);
		} else{
			response.status(404);
			return response.send("Cannot GET " + request.url);
		}
	});	

	//Add admin places - public
	app.post('/api/admindata', function(request, response){
		response.status(404);
		return response.send("Cannot POST " + request.url);
	});	

	//Add admin places - admin
	app.post('/api/admin/admindata', function(request, response){
		var cookie = null;
	
		if(request.cookies['user'] !== undefined){
			cookie = JSON.parse(request.cookies['user']);	
		}
		
		if(cookie != null && cookie.view == 0){
			return adminDataRouteFunctions.addAdminPlaces(request, response);
		} else{
			response.status(404);
			return response.send("Cannot POST " + request.url);
		}
	});	

	//Update admin places - public 
	app.put('/api/admindata', function(request, response){
		response.status(404);
		return response.send("Cannot PUT " + request.url);
	});

	//Update admin places - admin
	app.put('/api/admin/admindata', function(request, response){
		if(request.body.type == "cronJob"){
			adminDataRouteFunctions.updateAdminPlaces(request, response, io);
		} else{
			var cookie = null;
	
			if(request.cookies['user'] !== undefined){
				cookie = JSON.parse(request.cookies['user']);	
			}
		
			if(cookie != null && cookie.view == 0){
				return adminDataRouteFunctions.updateAdminPlaces(request, response, io);
			} else{
				response.status(404);
				return response.send("Cannot PUT " + request.url);
			}		
		}
	});		

	//Delete admin places - public (should not be able to do so)
	app.delete('/api/admindata', function(request, response){
		response.status(404);
		return response.send("Cannot DELETE " + request.url);
	});

	//Delete admin places - private
	app.delete('/api/admin/admindata', function(request, response){
		var cookie = null;
	
		if(request.cookies['user'] !== undefined){
			cookie = JSON.parse(request.cookies['user']);	
		}
		
		if(cookie != null && cookie.view == 0){
			return adminDataRouteFunctions.deleteAdminPlaces(request, response);
		} else{
			response.status(404);
			return response.send("Cannot DELETE " + request.url);
		}
	});
	
}