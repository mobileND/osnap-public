//Central Routes file
//Adds all other routes files
module.exports = function (app, io) {
	require("./rides")(app, io);
	require("./users")(app);
	require("./admindata")(app, io);
};
