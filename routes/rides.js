var rideRouteFunctions = require('./functions/ridesFunctions');

module.exports = function (app, io) {
	
	//Ride Request Routes
	//////////////////////////
	//Get list of all rides - public
	app.get('/api/rides', function(request,response){
		if(app.checkIP(request)){
			rideRouteFunctions.returnRides(request, response, "all");
		} else{
			response.status(404);
			return response.send("Cannot GET " + request.url);
		} 
	});

	//Get list of all rides - admin
	app.get('/api/admin/rides', function(request, response){	
		var cookie = null;
	
		if(request.cookies['user'] !== undefined){
			cookie = JSON.parse(request.cookies['user']);	
		}

		if(cookie != null && (cookie.view == 0 || cookie.view == 1)){
			return rideRouteFunctions.returnRides(request, response, "all");
		} else{
			response.status(404);
			return response.send("Cannot GET " + request.url);
		}
	});

	//Get a single ride - public
	app.get('/api/rides/:id', function(request, response){			
		if(app.checkIP(request)){
			rideRouteFunctions.returnSingleRide(request, response);
		} else{
			response.status(404);
			return response.send("Cannot GET " + request.url);
		} 
	});

	//Get a single ride - admin
	app.get('/api/admin/rides/:id', function(request, response){			
		var cookie = null;
	
		if(request.cookies['user'] !== undefined){
			cookie = JSON.parse(request.cookies['user']);	
		}
		
		if(cookie != null && cookie.view == 0){
			return rideRouteFunctions.returnSingleRide(request, response);
		} else{
			response.status(404);
			return response.send("Cannot GET " + request.url);
		}		
	});

	//Insert a new ride - public
	app.post('/api/rides', function(request, response){
		if(app.checkIP(request)){
			rideRouteFunctions.insertNewRide(request, response, io);
		} else{
			response.status(404);
			return response.send("Cannot POST " + request.url);
		}
	});	

	//Insert a new ride - admin
	app.post('/api/admin/rides', function(request, response){
		var cookie = null;
	
		if(request.cookies['user'] !== undefined){
			cookie = JSON.parse(request.cookies['user']);	
		}
		
		if(cookie != null && cookie.view == 0){
			return rideRouteFunctions.insertNewRide(request, response, io);
		} else{
			response.status(404);
			return response.send("Cannot POST " + request.url);
		}				
	});

	//Update a ride - public
	app.put('/api/rides/:id', function(request, response){	
		if(app.checkIP(request)){
			rideRouteFunctions.updateRide(request, response, io);
		} else{
			response.status(404);
			return response.send("Cannot PUT " + request.url);
		}	
	});

	//Update a ride - private
	app.put('/api/admin/rides/:id', function(request, response){	
		var cookie = null;
	
		if(request.cookies['user'] !== undefined){
			cookie = JSON.parse(request.cookies['user']);	
		}
		
		if(cookie != null && (cookie.view == 0 || cookie.view == 1)){
			return rideRouteFunctions.updateRide(request, response, io);
		} else{
			response.status(404);
			return response.send("Cannot PUT " + request.url);
		}	
	});	

	//Delete a ride - private
	app.delete('/api/admin/rides/:id', function(request, response){
		var cookie = null;

		if(request.cookies['user'] !== undefined){
			cookie = JSON.parse(request.cookies['user']);
		}

		if(cookie != null && (cookie.view == 0 || cookie.view == 1)){
			return rideRouteFunctions.deleteRide(request, response, io);
		} else{
			response.status(404);
			return response.send("Cannot DELETE " + request.url);
		}
	});

	//Check if Heavy Usage - public
	//Returns number of rides that are not complete
	app.get('/api/active/rides', function(request, response){	
		if(app.checkIP(request)){
			rideRouteFunctions.returnRides(request, response, "active");
		} else{
			response.status(404);
			return response.send("Cannot GET " + request.url);
		}	
	});
}
