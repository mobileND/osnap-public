var app = app || {};

app.Ride = Backbone.Model.extend({
	defaults: {
		location: 'empty',
		destination: 'empty',
		name: 'empty',
		numberPassengers: 0,
		phone: '',
		closed: false,
		cancelled: false,
		status: 0,
		assignedTo: "",
		log: "",
		showLog: true		
	},
	
	//See explanation in admindata.js
	parse: function(response){
		response.id = response._id;
		return response;
	},

});