var app = app || {};

app.AdminData = Backbone.Model.extend({
	defaults: {
		maxRiders: 4,
		locations: []
	},
	
	//This is done to create an 'id' attribute for the admindata model 
	//as well as each location in the location array. This is needed
	//because the database by default returns the models with an '_id'
	//attribute as opposed to a 'id' attribute. This is done as soon as
	//the model is loaded from the database 
	parse: function(response){
		response.id = response._id;
		for(var i = 0; i < response.locations.length; i++){
			response.locations[i].id = response.locations[i]._id;
		}
		return response;
	} 	
});