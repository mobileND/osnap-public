var app = app || {};

app.User = Backbone.Model.extend({
	defaults: {
		username: "",
		isAdmin: false,
		isDriver: false
	}
});