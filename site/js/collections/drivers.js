var app = app || {};

app.Drivers = Backbone.Collection.extend({
	model: app.User,
	url: '/api/drivers'
});