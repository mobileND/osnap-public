var app = app || {};

app.Rides = Backbone.Collection.extend({
	model: app.Ride,
	url: '/api/admin/rides',
		
	comparator: function (a,b) {
		if (a.attributes.createdAt > b.attributes.createdAt)
			return -1;
		if (a.attributes.createdAt < b.attributes.createdAt)
			return 1;
		return 0;
	},
	
	initialize: function(){
		var self = this;
	}	
});