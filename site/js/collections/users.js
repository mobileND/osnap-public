var app = app || {};

app.Users = Backbone.Collection.extend({
	model: app.User,
	url: '/api/admin/users',

	comparator: function (a,b) {
		if (a.attributes.username < b.attributes.username)
			return -1;
		if (a.attributes.username > b.attributes.username)
			return 1;
		return 0;
	}
});
