var app = app || {};
var CONFIG = {
	hostname: 'https://osnap.nd.edu/'
}
//This is the start of the application
//It decides what view to show initially 
//when the user goes to the website
$(function(){	

	var socketio = document.createElement('script');
	socketio.onload = function() {
		$.cookie.json = true;
		var user = $.cookie('user');
		
		if(user != null){
			//If view is initialized we know the user is an admin
			//as well as a driver so we must determined what the
			//last view they were in was
			if(user.view != null){
				if(user.view == 0){
					new app.RidesView;				
				}
				else{
					new app.DriverView;				
				}
			}
			else{
				if(user.isAdmin){
					new app.RidesView;
				}
				else if(user.isDriver){
					new app.DriverView;
				}			
			}
		}
		else{
			new app.LoginView;		
		}
	}
	document.body.appendChild(socketio).src= CONFIG.hostname + 'socket.io/socket.io.js';
	
});
