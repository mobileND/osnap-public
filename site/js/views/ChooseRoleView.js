var app = app || {};

//This class is used to handle the case of an 
//admin being both an admin and a driver
app.ChooseRoleView = Backbone.View.extend({
	el: '.content',
	
	template: _.template($('#choose-role-template').html()),
	
	events: {
		"submit form.choose-role-form" : "login",
	},

	initialize: function(options){
		this.render();
		this.options = options;
	},

	render: function(){
		this.$el.html(this.template);
	},
	
	login: function(e){
		e.preventDefault();		
		var selected = $('input[name="role"]:checked').val();

		//If selected == 0 that means the person is an admin
		if(selected == "0"){
			$.cookie('user', { name: this.options.data.username, isAdmin: true, isDriver: true, view: 0}, {expires: 7});						
			if(navigator.userAgent.match(/iPad/i)){
				new app.RidesView({login:true});
			} else {
				new app.RidesView();
			}
			this.undelegateEvents();
			delete this;	
		}
		else{
			$.cookie('user', { name: this.options.data.username, isAdmin: true, isDriver: true, view: 1}, {expires: 7});						
			if(navigator.userAgent.match(/iPad/i)){
				new app.DriverView({login:true});
			} else {
				new app.DriverView();
			}
			this.undelegateEvents();
			delete this;			
		}
	}
		
});
