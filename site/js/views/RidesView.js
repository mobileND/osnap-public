var app = app || {};

app.RidesView = Backbone.View.extend({
	el: '.content',
	
	template: _.template($('#requests-template').html()),

	events: {
		'change #requestStatus, #generalStatus' : 'filterByStatus',
		'click #genLogs' : 'generateLogs',
		'click .log-out' : 'logOut',
		'click .switchView' : 'switchView',
		'click span#next' : 'nextPage',
		'click span#prev' : 'prevPage',
		'click #liAdminData' : 'showAdminData',
		'click span#addNew' : 'addNewEntry',
		'click .user-info' : 'showUserInfo'		
	},	
	
	//Used to paginate rides
	pageNumber: 1,

	justRefreshed: false,
	
	maxPages: 0,

	lastPageNo: 0, //keeps track of last page no on a set

	previousStartNo: 0,
	
	//Max rides per page is 10
	resultsPerPage: 10,

	//Max page numbers displayed at once
	//at bottom of screen
	maxPageNoPerPage: 10,
	
	//Used to keep track of page numbers 
	//displayed at bottom of screen
	currentPageSet: 0,

	maxPageSet: 0,
	
	currentRequests: [],
	
	cancelledRequests: [],
	
	start: null,
	
	end: null,

	initialize: function(options){	
		//Change scale to fit iPad better - have to reload so meta tag can go back to original setting
		//the setting is changed when coming in from the login screen, which zooms in. However, if you
		//didn't come from the login page the setting remains the same so therefore no need to refresh
		if(options != null && options.login != null && options.login  == true){
			location.reload();
		}

		this.collection = new app.Rides();
		this.listenTo(this.collection, 'reset', this.render);
		var self = this;

		//Socket.io trigger automatic updates when new ride added
		var socket = io.connect(CONFIG.hostname);//global variable from config.js file
		socket.on('refresh', function (data) {
			self.collection.fetch({
				reset: true
			});
			
			self.justRefreshed = true;
			var audio = new Audio('audio/sound.mp3');
			audio.play();
		});

		socket.on('ride-update', function (data) {
			self.collection.fetch({
				reset: true
			});
		});
		
		//Need user list so you can edit permissions of the different users
		app.UserList = new app.Users();
		app.UserList.fetch({reset: true});
		app.AdminDataList = new app.AdminDataC();
		
		socket.on('cron-job', function(data){
			//This is done just in case someone is logged in when the cron job runs
			//If that's the case then the program will automatically update
			location.reload();
		});
		
		socket.on('servicestatus-change', function(data){
			app.AdminDataList.fetch({
				reset: true,
	
				success: function(){
					if(app.AdminDataList.models[0].attributes.serviceStatus == 1){
						$('#serviceRunning').css('display', 'block');
						$('#serviceStopped').css('display', 'none');	
						//This code below is for syncronization purposes
						//ie if someone happens to have the service status window open
						//when someone else changes the status from a different browser
						//this will keep everything updated...that's unlikely to occur
						//but you never know.
						if($('#serviceRadio1').length != 0 && !$('#serviceRadio1').is(':checked')){
							$('#serviceRadio1').prop('checked',true);
							$('#serviceRadio2').prop('checked',false);
						}												
					} else{
						$('#serviceRunning').css('display', 'none');
						$('#serviceStopped').css('display', 'block');		        			
						//See explanation in previous statement
						if($('#serviceRadio2').length != 0 && !$('#serviceRadio2').is(':checked')){
							$('#serviceRadio2').prop('checked',true);
							$('#serviceRadio1').prop('checked',false);
						}	
					}
				}							
			});		
		});
		
		var self = this;
		app.AdminDataList.fetch({
			reset: true,
			success: function(){
				self.$el.html(self.template);
				self.collection.fetch({reset: true});
			}
		});

	},

	render: function(){
		//Handle which icon to display based on if service is off or on
		if(app.AdminDataList.models[0].attributes.serviceStatus == 1){
			$('#serviceRunning').css('display','block');
		} else {
			$('#serviceStopped').css('display','block');
		}
		this.filterByStatus();
	},
	
	renderRide: function(ride, alt, cancelled){
		var rideView = new app.RideView({
			model: ride
		});
		if(cancelled){
			this.$('#requests').append(rideView.$el.attr('class', 'requestTableRow cancelled'));
		}
		else{
			if(alt) 
			{
				if($('#requestStatus').val() == "7")
				{
					switch (rideView.model.attributes.status) {
						case 0: {
							this.$('#requests').append(rideView.$el.addClass('requestTableRow openStatusNew'));
							break;
						}
						case 1: {
							this.$('#requests').append(rideView.$el.addClass('requestTableRow openStatusConfirmed'));
							break;
						}
						case 2: {
							this.$('#requests').append(rideView.$el.addClass('requestTableRow openStatusAccepted'));
							break;
						}
						case 3: {
							this.$('#requests').append(rideView.$el.addClass('requestTableRow openStatusPickedUp'));
							break;
						}
						default: {
							this.$('#requests').append(rideView.$el.attr('class', 'requestTableRow alt'));
							break;
						}
					}
				}
				else{
					this.$('#requests').append(rideView.$el.attr('class', 'requestTableRow alt'));
				}
			}
			else 
			{
				if($('#requestStatus').val() == "7")
				{
					switch (rideView.model.attributes.status) {
						case 0: {
							this.$('#requests').append(rideView.$el.addClass('requestTableRow openStatusNew'));
							break;
						}
						case 1: {
							this.$('#requests').append(rideView.$el.addClass('requestTableRow openStatusConfirmed'));
							break;
						}
						case 2: {
							this.$('#requests').append(rideView.$el.addClass('requestTableRow openStatusAccepted'));
							break;
						}
						case 3: {
							this.$('#requests').append(rideView.$el.addClass('requestTableRow openStatusPickedUp'));
							break;
						}
						default: {
							this.$('#requests').append(rideView.$el.attr('class', 'requestTableRow'));
							break;
						}
					}
				}
				else{
					this.$('#requests').append(rideView.$el.attr('class', 'requestTableRow'));			
				}
			}
		}
	},
	
	renderPage: function(){
		if(this.maxPages > 1 && this.pageNumber != this.maxPages) $('li#next').css('display', 'inline-block');			
		else $('li#next').css('display', 'none');			
		
		
		if(this.pageNumber == 1) $('li#prev').css('display', 'none');
		else $('li#prev').css('display', 'inline-block');						
		
		var count = 0;
		var start = this.pageNumber * this.resultsPerPage - this.resultsPerPage;
		var end = this.pageNumber * this.resultsPerPage; 
		for( var i = start; (i < end) && (i < this.currentRequests.length); i++){
			if(count % 2 == 0){
				if(this.currentRequests[i].attributes.status == 6 && this.currentRequests[i].attributes.cancelled != true){
					this.renderRide(this.currentRequests[i], false, true);					
				}
				else this.renderRide(this.currentRequests[i], false, false);				
			}
			else{
				if(this.currentRequests[i].attributes.status == 6 && this.currentRequests[i].attributes.cancelled != true){
					this.renderRide(this.currentRequests[i], true, true);					
				}
				else this.renderRide(this.currentRequests[i], true, false);					
			}
			count++;			
		}
	},
	
	//Used to sort rides by timestamp -- Most recent rides at top of screen
	sortFunction: function (a,b) {
		var date1 = new Date(a.attributes.date + ' ' + a.attributes.time);
		var date2 = new Date(b.attributes.date + ' ' + b.attributes.time);
		if( date1 > date2){
			return -1;
		} else if ( date1 < date2 ){
			return 1;
		} else {
			return 0;
		}
	},	
	
	filterByStatus: function(){
		var self = this;
		this.currentRequests = [];
		var newRequests = [];
		var confirmedRequests = [];
		var acceptedRequests = [];
		var pickedUp = [];

		$('.requestTableRow').remove();
		$('#buttonBarTable').empty(); //always clear the page list before adding to it
		var count = 0;
		var requestStatus = this.$('#requestStatus').val();
		this.collection.each(function(ride){
			if(requestStatus == "7"){
				if(ride.attributes.status == "0"){
					newRequests.push(ride);		
				}	
				else if(ride.attributes.status == "1"){
					confirmedRequests.push(ride);
				}
				else if(ride.attributes.status == "2"){
					acceptedRequests.push(ride);
				}
				else if(ride.attributes.status == "3"){
					pickedUp.push(ride);
				}
				else if(ride.attributes.status == "6" && ride.attributes.cancelled != true){
					this.cancelledRequests.push(ride);
				}
			}
			else{
				if(ride.attributes.status == requestStatus){
					this.currentRequests.push(ride);
				}
				else if(ride.attributes.status == "6" && ride.attributes.cancelled != true){
					this.cancelledRequests.push(ride);
				}
			}
		}, this);
			
		this.cancelledRequests.sort(this.sortFunction);
		this.currentRequests.sort(this.sortFunction);
		
		if(requestStatus == "7"){
			if(pickedUp != null){
				pickedUp.sort(this.sortFunction);
				this.currentRequests = pickedUp.concat(this.currentRequests);
			}
			if(acceptedRequests != null){
				acceptedRequests.sort(this.sortFunction);
				this.currentRequests = acceptedRequests.concat(this.currentRequests);
			}
			if(confirmedRequests != null){
				confirmedRequests.sort(this.sortFunction);
				this.currentRequests = confirmedRequests.concat(this.currentRequests);
			}
			if(newRequests != null){
				newRequests.sort(this.sortFunction);
				this.currentRequests = newRequests.concat(this.currentRequests);
			}
		}

		this.currentRequests = this.cancelledRequests.concat(this.currentRequests);
		this.cancelledRequests = [];

		//does this so that if a refresh socket.io event happens the table still
		//stays on the same page that it was previously on
		if(this.justRefreshed == false){
			this.pageNumber = 1;
		} else {
			this.justRefreshed = false;
		}
		this.maxPages = Math.ceil(this.currentRequests.length / this.resultsPerPage);
		
		this.renderPage();

		if( this.maxPages > 1) {
			this.currentPageSet = 1;
			//Reset variables
			this.lastPageNo = 0;
			this.previousStartNo =  0;
			this.displayPageNumberSet();
		}
		
		//Set up click handlers after page has rendered. These involve changing status of 
		//OSNAP service when they are clicked 
		$('#stopButton').click(function(){
			$.ajax({
				type: "PUT",
				url: "/api/admin/admindata",
				data: {serviceStatus: 1},
				statusCode: {
					500: function() {
						var notifier = window.notifier = new Backbone.Notifier({
							ms: 2000,
							el: 'body',
							theme: 'plastic',
							type: 'error',
							position: 'top'
						});
						notifier.notify("Failed to update service status. Please try again!");
					},
					200: function() {
						var notifier = window.notifier = new Backbone.Notifier({
							ms: 2000,
							el: 'body',
							theme: 'plastic',
							type: 'success',
							position: 'top'
						});
						notifier.notify("Successfully updated service status!");

						$('#serviceRunning').css('display', 'block');
						$('#serviceStopped').css('display', 'none');
						app.AdminDataList.fetch({ reset: true });
					}
				}
			});
		});

		$('#startButton').click(function(){
			$.ajax({
				type: "PUT",
				url: "/api/admin/admindata",
				data: {serviceStatus: 0},
				statusCode: {
					500: function() {
						var notifier = window.notifier = new Backbone.Notifier({
							ms: 2000,
							el: 'body',
							theme: 'plastic',
							type: 'error',
							position: 'top'
						});
						notifier.notify("Failed to update service status. Please try again!");
					},
					200: function() {
						var notifier = window.notifier = new Backbone.Notifier({
							ms: 2000,
							el: 'body',
							theme: 'plastic',
							type: 'success',
							position: 'top'
						});
						notifier.notify("Successfully updated service status!");

						$('#serviceRunning').css('display', 'none');
						$('#serviceStopped').css('display', 'block');
						app.AdminDataList.fetch({ reset: true });
					}
				}
			});
		});
	},

	displayPageNumberSet: function(flag){

		//This next if is to ensure you don't have cases like the app displaying page numbers
		// 1-10 and then having '...11' which wouldn't make sense. This ensures that if there
		// are only 10 pages then all 10 will display, but otherwise it'll only display '1-7 ... [Last Page #]'

		if( this.currentPageSet == 1){
			if( (this.maxPages - this.currentPageSet*8) == 1 || (this.maxPages - this.currentPageSet*8) == 2){
				this.maxPageNoPerPage = 10;
			} 
			else if ((this.maxPages - this.currentPageSet*10) == 1) this.maxPageNoPerPage = 8;
			else if ((this.maxPages - this.currentPageSet*8 ) <= 0) this.maxPageNoPerPage = 8;	
			else if ((this.maxPages - this.currentPageSet*10 ) <= 0) this.maxPageNoPerPage = 10;
			else this.maxPageNoPerPage = 8;
			
			this.maxPageSet = Math.ceil(this.maxPages/this.maxPageNoPerPage);
		}
		else if ( this.currentPageSet == this.maxPageSet && flag != 1){
			this.maxPageNoPerPage = 10;
			//max page number does not change here because it shouldn't change if you've already reached the max number of pages
		}
		else{
			if( (this.maxPages - ((this.currentPageSet - 1)*6 + 8)) == 1 || (this.maxPages - ((this.currentPageSet - 1)*6 + 8)) == 2){
				//the plus 8 is added above to take in consideration that the first page set always displays 8 pages on it
				this.maxPageNoPerPage = 8;
			} 
			else this.maxPageNoPerPage = 6;
			
			if(flag != 1){
				//This is only done if moving forward because not necessary to do so when moving back
				var pagesLeft = this.maxPages - this.lastPageNo;
				this.maxPageSet = this.currentPageSet - 1 + Math.ceil(pagesLeft/this.maxPageNoPerPage);
			}
		}	

		var self = this;
		var start = 0;
		var end = 0;
		if(this.lastPageNo == 0){
			start = this.currentPageSet * this.maxPageNoPerPage - this.maxPageNoPerPage;
			end = start + this.maxPageNoPerPage;
			this.lastPageNo = end;
			this.previousStartNo = start;
		} 
		else if (flag != null){
			end = this.previousStartNo;
			start = end - this.maxPageNoPerPage;
			this.lastPageNo = end;
			this.previousStartNo = start;
		}
		else {
			start = this.lastPageNo;
			end = start + this.maxPageNoPerPage;
			if(end > this.maxPages) end = this.maxPages;
			this.lastPageNo = end;
			this.previousStartNo = start;
		}

		$('#buttonBarTable').empty(); //always clear the page list before adding to it

		if(this.currentPageSet > 1){
			$('#buttonBarTable').append('<li class="pageNo" id="previousPageNoSet"><a class="previousPageNoSet uiButton"><span><</span></a></li>');
			$('#buttonBarTable').append('<li class="pageNo" id="firstPage"><a class="uiButton"><span>1</span></a></li>');
			$('#buttonBarTable').append('<li class="ellipsis">…</li>');
			
			$('.previousPageNoSet').click(function(){
				self.currentPageSet--;
				self.displayPageNumberSet(1);
			});
		}

		for(var i = start; (i < end) && (i < this.maxPages); i++){
			var id = 'pageLi' + (i+1);
			if(i == 0){
				//This is done to highlight the first page number immediately
				//because we are initially on page one
				$('#buttonBarTable').append('<li class="pageNo" id=' + id + '><a class="pageNoSelected uiButton"><span>' + (i+1) + '</span></a></li>');
			} else {
				$('#buttonBarTable').append('<li class="pageNo" id=' + id + '><a class="uiButton"><span>' + (i+1) + '</span></a></li>');
			}
		}

		if(this.maxPageSet > this.currentPageSet){
			$('#buttonBarTable').append('<li class="ellipsis">…</li>');
			$('#buttonBarTable').append('<li class="pageNo" id="finalPage"><a class="uiButton"><span>' + this.maxPages + '</span></a></li>');
			$('#buttonBarTable').append('<li class="pageNo" id="nextPageNoSet"><a class="nextPageNoSet uiButton">></a></li>');

			$('.nextPageNoSet').click(function(){
				self.currentPageSet++;
				self.displayPageNumberSet();
			});
		}

		this.highlightPageNumber(this.pageNumber);

		$('.pageNo').click(function(e){
			self.pageNumber = parseInt(e.target.firstChild.textContent);
			self.highlightPageNumber(self.pageNumber);
			$('.requestTableRow').remove();
			self.renderPage();
		});

	},
	
	highlightPageNumber: function(pageNumber){
		//Remove pageNoSelected class first so the button highlight will go away
		$('.pageNoSelected').removeClass('pageNoSelected');
		
		$('.pageNo').each(function() {
			if($(this).find('a').text() == pageNumber){
				$(this).find('a').addClass('pageNoSelected');
				return false; //break loop
			}	
		});
	},
	
	//Next few functions get super messy because I am dealing with a cloned dialog
	//When dialog is cloned if there are elements with the same ids then things get messed up
	//Perhaps look into a better solution in the future
	
	showAdminData: function(){
		var cloneDialog = this.$('#adminInfo').clone();
		var self = this;
		cloneDialog.dialog({
			width: "auto",
			
			open: function (event, ui){
				$(this).find('.tabs')
					.attr('id', 'tabs')
					.tabs()
					.addClass('ui-tabs-vertical ui-helper-clearfix');
				
				$(this).find('.exitAdminInfo').attr('id', 'exitAdminInfo');
	
				var tab1 = $(this).find('#fragment-1');
				//Below you need to create IDs because this dialogue box neds to be a copy of the original and you want to make the copy elements unique
				tab1.find(".locations").attr('id', 'locations');
				tab1.find(".activePlaces").attr('id', 'activePlaces');
				tab1.find(".disableButton").attr('id', 'disableButton');					 
				tab1.find(".deleteButton").attr('id', 'deleteButton');
				tab1.find(".editButton").attr('id', 'editButton');
				tab1.find(".editButtonDisabled").attr('id', 'editButtonDisabled');				
				tab1.find(".disabledPlaces").attr('id', 'disabledPlaces');
				tab1.find(".enableButton").attr('id', 'enableButton');						 
				tab1.find(".deleteButtonDisabled").attr('id', 'deleteButtonDisabled');							 
				tab1.find(".newLocationName").attr('id', 'newLocationName');
				tab1.find(".newLocationForm").attr('id', 'newLocationForm');
				tab1.find(".newLocationDes").attr('id', 'newLocationDes');	
				tab1.find(".newShortName").attr('id', 'newShortName');

				$('#exitAdminInfo').click(function(){
					cloneDialog.dialog("close");
				});
				
				$('#editButton, #editButtonDisabled').click(function(e){
					//open dialog box for editing individual locations
					var editDialog = $('#editLocation').clone();					
					var locationName, description, shortName;
				
					if(e.target.id == 'editButton') locationName = $('#activePlaces').val();
					else locationName = $('#disabledPlaces').val();
				 
					$.each(app.AdminDataList.models[0].attributes.locations, function(index, object){
						if(object.name == locationName){
							description = object.description;
							shortName = object.shortName;
							return false; 
						}
					});
						
					editDialog.dialog({
						width: "auto",
					
						open: function (event, ui){
							var form = $(this);
							//Did line above because needed to eventually get value of textarea and jquery modal dialoge and textareas are not easily compatible
							$(this).find(".elfName").attr('id', 'elfName').text(locationName);									
							$(this).find(".elfSuccess").attr('id', 'elfSuccess');																	
							$(this).find(".elfDescription").attr('id', 'elfDescription');
							$(this).find(".elfShortName").attr('id','elfShortName');
							$(this).find('#elfDescription').text(description);
							$(this).find('#elfShortName').val(shortName);
							$(this).find('.closeEditIpad').attr('id', 'closeEditIpad');
					
							$(this).find('.editLocationForm').attr('id', 'editLocationForm').submit(function(e){
								//Handle submits of the Edit Location Form
								e.preventDefault();
								$.ajax({
									type: "PUT",
									url: "/api/admin/admindata",
									data: {name: locationName, description: form.find('#elfDescription').val(), shortName: form.find('#elfShortName').val()},
									statusCode: {
										500: function() {
											var notifier = window.notifier = new Backbone.Notifier({
												ms: 2000,
												el: '#editLocationForm',
												theme: 'plastic',
												type: 'error',
												position: 'bottom'
											});
											notifier.notify("Failed to edit location info. Please try again.");
										},
										200: function() {
											var notifier = window.notifier = new Backbone.Notifier({
												ms: 2000,
												el: '#editLocationForm',
												theme: 'plastic',
												type: 'success',
												position: 'bottom'
											});
											notifier.notify("Successfully updated location info.");	

											app.AdminDataList.fetch({
												reset: true
											});				
										}
									}					
								});
							});

							$('#closeEditIpad').click(function(e){
								e.preventDefault();
								editDialog.dialog("close");
							});
						},
					
						close: function (event, ui){
							$(this).dialog("destroy");
							$(this).remove();							
						}
						
					}).prev(".ui-dialog-titlebar").css("background",'#8C8C8C');
					return false;
				});			
				
				//Create other tabs for the popup dialogue box
				var maxPassengersTab = $(this).find('#fragment-3');
				maxPassengersTab.find(".maxPassengersNumber").attr('id', 'maxPassengersNumber');
				maxPassengersTab.find(".updateButton").attr('id', 'updateButton');	
				maxPassengersTab.find(".successPassengers").attr('id', 'successPassengers');

				var serviceStatusTab = $(this).find('#fragment-4');
				serviceStatusTab.find(".serviceRadio1").attr('id', 'serviceRadio1');
				serviceStatusTab.find(".serviceRadio2").attr('id', 'serviceRadio2');
				//Create corresponding view	
				new app.AdminDataView();

				var usersTab = $(this).find('#fragment-2');
				usersTab.find(".users").attr('id', 'users');
				usersTab.find(".userAttributes").attr('id', 'userAttributes');			
				usersTab.find(".isAdmin").attr('id', 'isAdmin');
				usersTab.find(".isDriver").attr('id', 'isDriver');					 
				usersTab.find(".deleteButton").attr('id', 'deleteButton');
				usersTab.find(".cancel").attr('id', 'cancel');	
				usersTab.find(".success").attr('id', 'success');					
				//Create corresponding view			
				new app.ManageAccessView();				

			},
			
			close: function (event, ui){
				$(this).dialog("destroy");
				$(this).remove();
			}
			
		}).prev(".ui-dialog-titlebar").css("background",'#8C8C8C');
	},
	
	//Add a ride request manually 
	addNewEntry: function(){
		var cloneDialog = this.$('#addNewView').clone();
		var self = this;
		cloneDialog.dialog({
			width: "auto",
			
			open: function (event, ui){			

				$(this).find('.addNewForm').attr('id', 'addNewForm').submit(function(e){						
					e.preventDefault();
					//validates phone number entry
					if(self.validatePhone($('#anfPhone').val())){
						$.ajax({
							type: "POST",
							url: "/api/admin/rides",
							data: {	
								name: $('#anfName').val(), 
								location: $('#anfLocation').val(),
								destination: $('#anfDestination').val(),
								passengers: $('#anfPassengers').val(),
								phone: $('#anfPhone').val()
							}
						}).done(function(){
							cloneDialog.dialog("close");
						});									
					}
					else{
						$('#anfPhoneError').css('display', 'block');
					}																			
				});

				$(this).find('.exitNewRequest').attr('id', 'exitNewRequest');			
				$(this).find('.anfName').attr('id', 'anfName');
				$(this).find('.anfLocation').attr('id', 'anfLocation');	
				$(this).find('.anfDestination').attr('id', 'anfDestination');
				$(this).find('.anfPassengers').attr('id', 'anfPassengers');		
				$(this).find('.anfPhone').attr('id', 'anfPhone').focus(function(e){
					$('#anfPhoneError').css('display', 'none');					
				});
						
				$(this).find('.anfPhoneError').attr('id', 'anfPhoneError');
				$('#exitNewRequest').click(function(e){
					e.preventDefault();
					cloneDialog.dialog("close");
				});
				
				var data = app.AdminDataList.models[0].attributes.locations;
				var maxRiders = app.AdminDataList.models[0].attributes.maxRiders;				
				for(var i = 0; i < data.length; i++){
					if(data[i].disabled == false){
						$('#anfLocation, #anfDestination').append($('<option></option>')
						   .attr('value', data[i].name)
						   .text(data[i].name));				
					}	
				}	
				
				for(var i = 1; i < maxRiders + 1; i++){
					$('#anfPassengers').append($('<option></option>')
						.attr('value', i)
						.text(i));
				}															

			},
			
			close: function (event, ui){
				$(this).dialog("destroy");
				$(this).remove();
			}
			
		}).prev(".ui-dialog-titlebar").css("background",'#8C8C8C');		
	},
	
	generateLogs: function(){	
		var cloneDialog = this.$('#allLogs').clone();
		var self = this;
		cloneDialog.dialog({
			
			width: "auto",
			
			open: function (event, ui) {
				$(this).find('.export').attr('id', 'export').click(function(){	
					var text = $('#resultsText').text().toString();
					var textCopy = text;
					text = text.split(' ');

					var _el = $('<div></div>');
					_el.append($('#resultsText').html());
					var p = $('p', _el);
					var li = $('li',_el);
					var heading = $('h5', _el);

					var formattedContent, title;

					if($("#logType:checked").val() == "0"){
						title = "log_" + self.start + "--" + self.end + '.csv';	
						formattedContent = 'Name,Location,Destination,Passengers,Phone,Time,Date,AssignedTo,DeltaAssignedTo,DeltaAssignedToFinished,DeltaStartFinish,Current Status' + 
						'break' + $(heading[0]).text() + 'break';				
					}else{
						title = "stats_" + self.start + "--" + self.end + '.txt';						
						formattedContent = $(heading[0]).text() + 'break';		
					}

					for(var i = 0; i < p.length; i++){
						if($(p[i]).has('b').length){
							formattedContent += ($(p[i]).text() + 'break');							
						}
						else{
							formattedContent += ('tab-' + $(p[i]).text() + 'break');							
						}
					}

					for(var j = 0; j < li.length; j++){
						formattedContent += ('tab-' + $(li[j]).text() + 'break');
					}

					var data = {
						name: title,
						content: formattedContent
					};

					var file = null;
					if($("#logType:checked").val() != "0"){
						file = formattedContent;
						file = file.replace(/break/g, "\n");
						file = file.replace(/tab-/g, "\t");
					}else {
						file = self.JSON2CSV(self.exportJSON);
					}
					var downloadLink = document.createElement("a");
					var blob = new Blob([file],{encoding:"UTF-8",type:"text/plain;charset=UTF-8"});
					var url = URL.createObjectURL(blob);
					downloadLink.href = url;
					downloadLink.download = data.name;

					document.body.appendChild(downloadLink);
					downloadLink.click();
					document.body.removeChild(downloadLink);
					self.exportJSON = [{"Name": "Name","Location": "Location","Destination": "Destination","Passengers": "Passengers","Phone": "Phone","Time": "Time","Date":"Date","AssignedTo": "AssignedTo","DeltaAssignedTo": "DeltaAssignedTo","DeltaAssignedToFinished": "DeltaAssignedToFinished","DeltaStartFinish": "DeltaStartFinish","Status": "Current Status"}]; //reset array
				});
				
				$(this).find('input[name=logType]').attr('id', 'logType').change(function(){
					$('#resultsText').empty();
					$('#export').css('display', 'none');
				});
				
				$(this).find('.exitGenLogs').attr('id','exitGenLogs');
				$(this).find('.results').attr('id', 'results');
				$(this).find('.resultsText').attr('id', 'resultsText'); 
				$(this).find('.generateLogButton').attr('id','generateLogButton').click(function(){self.returnLogs();});
				$(this).find('.customTimeFrame').attr('id', 'customTimeFrame');
				$(this).find('.timeFrame').attr('id', 'timeFrame').change(function(){self.handleTimeFrame();});
				$(this).find('.start').attr('id', 'startDate').datepicker();
				$(this).find('.end').attr('id', 'endDate').datepicker();
				$('#exitGenLogs').click(function(){
					cloneDialog.dialog("close");
				});
			},		
			close: function (event, ui) {
				$('#generateLogButton').datepicker("destroy");
				$('#startDate').datepicker("destroy");
				$('#endDate').datepicker("destroy");
				$('#timeFrame').datepicker("destroy");
				$('#customTimeFrame').datepicker("destroy");
				$('#resultsText').empty();
				$(this).dialog("destroy");
				$(this).remove();
			}				
		}).prev(".ui-dialog-titlebar").css("background",'#8C8C8C');
	},

	JSON2CSV: function(objArray) {
		var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
		var str = '';
		var line = '';

		if ($("#labels").is(':checked')) {
			var head = array[0];
			if ($("#quote").is(':checked')) {
				for (var index in array[0]) {
					var value = index + "";
					line += '"' + value.replace(/"/g, '""') + '",';
				}
			} else {
				for (var index in array[0]) {
					line += index + ',';
				}
			}
			line = line.slice(0, -1);
			str += line + '\r\n';
		}

		for (var i = 0; i < array.length; i++) {
			var line = '';

			if ($("#quote").is(':checked')) {
				for (var index in array[i]) {
					var value = array[i][index] + "";
					line += '"' + value.replace(/"/g, '""') + '",';
				}
			} else {
				for (var index in array[i]) {
					line += array[i][index] + ',';
				}
			}

			line = line.slice(0, -1);
			str += line + '\r\n';
		}
		return str;
	},
	
	handleTimeFrame: function(){
		$('#resultsText').empty();
		$('#export').css('display', 'none');
		if($('#timeFrame').val() == "5"){
			$('#customTimeFrame').css('display', 'block');
		}
		else $('#customTimeFrame').css('display', 'none');
	},
	
	returnLogs: function(){
		//This function generates logs based on a time frame
		//selected by the user
		//
		// Different Cases:
		//
		//	0 is rides Today
		//	1 is rides Yesterday
		//	2 is rides in Last 7 Days (including today)
		//	3 is rides in Last 14 days (including today)
		//	4 is rides in Last month (including today)
		//	5 is custom range entered by the user
		
		var logType = $("#logType:checked").val();
		switch($('#timeFrame').val()){
			//#timeFrame is the select statement in the generate logs popup
			case "0":{
				var today = new Date();
				today.setHours(7,0,0,0);
				today = new Date(today);
				var tomorrow = new Date();
				tomorrow.setDate(today.getDate() + 1);		
				tomorrow.setHours(6,59,59,999);
				
				if(logType == "0"){
					this.generateLogsByDate(today, tomorrow);						
				}
				else if(logType == "1"){
					this.generateStatisticsByDate(today, tomorrow);
				}
				break;			
			}
			case "1":{
				var yesterday = new Date();
				yesterday.setDate(yesterday.getDate() - 1);
				yesterday.setHours(7,0,0,0);
				yesterday = new Date(yesterday);
				var today = new Date();		
				today.setHours(6,59,59,999);
							
				if(logType == "0"){
					this.generateLogsByDate(yesterday, today);						
				}
				else if(logType == "1"){
					this.generateStatisticsByDate(yesterday, today);
				}
				break;			
			}			
			case "2":{
				var today = new Date();
				today.setHours(6,59,59,999);
				var lastWeek = new Date(today.getTime() - 7*24*60*60*1000);
				lastWeek.setHours(7,0,0,0);
				today.setDate(today.getDate() + 1);
								
				if(logType == "0"){
					this.generateLogsByDate(lastWeek, today);						
				}
				else if(logType == "1"){
					this.generateStatisticsByDate(lastWeek, today);					
				}
				break;				
			}
			case "3":{
				var today = new Date();
				today.setHours(6,59,59,999);
				var twoWeeks = new Date(today.getTime() - 14*24*60*60*1000);
				twoWeeks.setHours(7,0,0,0);
				today.setDate(today.getDate() + 1);

				if(logType == "0"){
					this.generateLogsByDate(twoWeeks, today);						
				}
				else if(logType == "1"){
					this.generateStatisticsByDate(twoWeeks, today);					
				}			
				break;
			} 
			case "4":{
				var date = new Date();
				date.setHours(6,59,59,999);
				var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
				var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
				firstDay.setHours(7,0,0,0);
				lastDay.setDate(lastDay.getDate() + 1);
				lastDay.setHours(6,59,59,999);
				
				if(logType == "0"){
					this.generateLogsByDate(firstDay, lastDay);						
				}
				else if(logType == "1"){
					this.generateStatisticsByDate(firstDay, lastDay);					
				}
				break;
			}
			case "5":{
				var start = new Date($('#startDate').val());
				var end = new Date($('#endDate').val());
				start.setHours(7,0,0,0);
				end.setDate(end.getDate() + 1);
				end.setHours(6,59,59,999);
				
				if(logType == "0"){
					this.generateLogsByDate(start, end);						
				}
				else if(logType == "1"){
					this.generateStatisticsByDate(start, end);					
				}
				break;
			}
		}
	},
	
	generateStatisticsByDate: function(start, end){
		var created = 0, 
			totalPpl = 0;
			delivered = 0,
			noShow = 0,
			cancelled = 0,
			people = 0,
			noShowPpl = 0,
			cancelledPpl = 0;
			
		this.collection.each(function(ride){
			if((new Date(ride.attributes.date + " " +  ride.attributes.time) >= new Date(start)) && (new Date(ride.attributes.date + " " + ride.attributes.time) <= new Date(end))){
				created++;
				totalPpl += ride.attributes.numberPassengers;
				if(ride.attributes.status == 5){
					delivered++;
					people+= ride.attributes.numberPassengers;
				}
				else if (ride.attributes.status == 4 ){
					noShow++;
					noShowPpl += ride.attributes.numberPassengers;
				}
				else if (ride.attributes.status == 6){
					cancelled++;
					cancelledPpl += ride.attributes.numberPassengers;
				}
			}			
		}, this);
		
		$.format.date(start, 'MMM dd, yyyy hh:mm:ss a');
		$.format.date(end, 'MMM dd, yyyy hh:mm:ss a');
		
		$('#results').css("display", "block");		
		var self = $('#resultsText');
		self.empty();					
		self.append('<h5>Statistics from ' + start.toLocaleString() + ' to ' + end.toLocaleString() +': </h5>');
		self.append('<div><p>' + created + ' rides requested ( ' + totalPpl + ' ' + this.pplOrPerson(totalPpl)  + ' ).</p><ul><li>' + delivered + ' rides delivered ( ' + people + ' ' + this.pplOrPerson(people) + ' ).</li><li>' + noShow + ' no show rides ( ' + noShow + ' ' + this.pplOrPerson(noShow) + ' ).</li><li>' + cancelled + ' rides cancelled ( ' + cancelled + ' ' + this.pplOrPerson(cancelled) + ' ).</li></ul></div>');
		 
		this.start = $.format.date(start, "MM-dd-yyyy");
		this.end = $.format.date(end, "MM-dd-yyyy")			
		
		$('#export').css('display', 'block');		
	},

	pplOrPerson: function(numberOfPpl){
		//Simple function used to determine whether or not
		//Person or people should be used when describing a 
		//specific number. Not essential, but is a nice touch for the app
		var personOrPeople = ["person","people"];
		var noun = "";
		numberOfPpl == 1 ? noun = personOrPeople[0]: noun = personOrPeople[1];
		return noun;
	},
	
	//Used to calculate changes in timestamps
	//Used to give additional information in the exported spreadsheets
	calculateDelta: function(days, seconds){
		var hours = parseInt( seconds / 3600 ) % 24;
		hours = hours + days*24;
		var minutes = parseInt( seconds / 60 ) % 60;
		var seconds = seconds % 60;

		return (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
	},

	exportJSON: [{"Name": "Name","Location": "Location","Destination": "Destination","Passengers": "Passengers","Phone": "Phone","Time": "Time","Date":"Date","AssignedTo": "AssignedTo","DeltaAssignedTo": "DeltaAssignedTo","DeltaAssignedToFinished": "DeltaAssignedToFinished","DeltaStartFinish": "DeltaStartFinish","Status": "Current Status"}],

	generateLogsByDate: function(start, end){
		var self = this;
		var message = new Array();
		message.push("Here are the rides generated today");
		this.collection.each(function(ride){
			var creationTime = new Date(ride.attributes.date + ' ' + ride.attributes.time);
			if((creationTime >= new Date(start)) && (creationTime <= new Date(end))){			
				var assignedDriverTime, finishedTime = "";
				var assignedDelta = "";
				var assignedFinishedDelta = ""; 
				var startFinishDelta = "";
				var skip = false;			
			
				$.each(ride.attributes.log, function(key, value){
					if(value.search("driver") != -1 && skip == false){
						assignedDriverTime = value.split(";")[3];
						skip = true;
					}
					else if(value.search("status") != -1 && (value.search(";4;") != -1 || value.search(";5;") != -1)){
						finishedTime = value.split(";")[3];
						return false;
					} 
					else if(value.search("cancelled") != -1){
						finishedTime = value.split(': ')[1];
						return false;
					}
				});
			
				if(assignedDriverTime){
					assignedDriverTime = assignedDriverTime.replace(',', ' ');
					assignedDriverTime = new Date(assignedDriverTime);
					assignedDelta = assignedDriverTime.getTime() - creationTime.getTime();
					assignedDelta = this.calculateDelta(assignedDriverTime.getDate() - creationTime.getDate(), assignedDelta / 1000);
				}
			
				if(finishedTime != ""){
					finishedTime = finishedTime.replace(',', ' ');
					finishedTime = new Date(finishedTime);
					if(assignedDriverTime){
						assignedFinishedDelta = (finishedTime.getTime() - assignedDriverTime.getTime());
						assignedFinishedDelta = this.calculateDelta(finishedTime.getDate() - assignedDriverTime.getDate(), assignedFinishedDelta / 1000);						
					}
					startFinishDelta = this.calculateDelta(finishedTime.getDate() - creationTime.getDate(), (finishedTime.getTime() - creationTime.getTime())/1000);
				}
						
				var msg = ride.attributes.name + ',' + ride.attributes.location + ',' + ride.attributes.destination + ',' + ride.attributes.numberPassengers +
				',' + ride.attributes.phone + ',' + ride.attributes.time + ',' + ride.attributes.date + ',' + (ride.attributes.assignedTo || "n/a") + ',' + 
				assignedDelta + ',' + assignedFinishedDelta + ',' +  startFinishDelta + ',' + this.numberToStatus(ride.attributes.status);
				message.push(msg);

				var json = {"name": ride.attributes.name, "location": ride.attributes.location, "destination": ride.attributes.destination, "numberPassengers": ride.attributes.numberPassengers, "phone": ride.attributes.phone, "time": ride.attributes.time, "date": ride.attributes.date, "assignedTo": (ride.attributes.assignedTo || "n/a"), "assignedDelta": assignedDelta, "assignedFinishedDelta": assignedFinishedDelta, "startFinishDelta": startFinishDelta, "status": this.numberToStatus(ride.attributes.status)};
				self.exportJSON.push(json);
			}
		}, this);
		
		this.start = $.format.date(start, "MM-dd-yyyy");
		this.end = $.format.date(end, "MM-dd-yyyy")	

		if(message.length > 0){
			$('#results').css("display", "block");
			var self = $('#resultsText');
			self.empty();
			for(var i = 0; i < message.length; i++){
				message[i] = this.parseLogEntry(message[i]);
				if(i != 0){
					if(message[i].search("Driver") == -1 && message[i].search("Request") == -1 && message[i].search("Status") == -1 && message[i].search("cancel") == -1){
						$('#resultsText').append('<p><b>' + message[i] +'</b></p>');
					}
					else{
						$('#resultsText').append('<p style=" text-indent: 2em;">' + message[i] +'</p>');						
					}
				}
			}
		}
		
		$('#export').css('display', 'block');
	}, 

	//Parses logs stored on the server 
	parseLogEntry: function(message){
		//Split the messages on semicolons
		//The first value will be either status, closed, driver or cancelled, 
		//which refers to the event represented by the log
		var values = message.split(';');
		if(values.length == 1){
			return values[0];
		}
		else{
			switch(values[0]){
				case "status":{
					return 'Status Change: "' + this.numberToStatus(parseInt(values[1])) + '" to "' + this.numberToStatus(parseInt(values[2])) + '" -- ' + values[3];
				}
				case "closed":{
					if(values[1] == "false"){
						return 'Request changed from "open" to "closed" -- ' + values[3];					
					}	
					else{
						return 'Request changed from "closed" to "open" -- ' + values[3];						
					}		
				} 
				case "driver":{
					if(values[1] == null || values[1] == ""){
						return 'Driver "' + values[2] + '" has been assigned to this ride -- ' + values[3];					
					}
					else{
						return 'Driver change: "' + values[1] + '" to "' + values[2] + '" -- ' + values[3];					
					}
				}
				case "cancelled":{
					return values[1].toString();
				}
			}			
		}
	},
	
	//Converts number status field of object to text
	numberToStatus: function(number){
		switch(number){
			case 0:{
				return "New";
			}
			case 1:{
				return "Confirmed by Admin";
			}
			case 2:{
				return "Accepted by Driver";
			}
			case 3:{
				return "Picked up";
			}
			case 4:{
				return "No show";
			}
			case 5:{
				return "Delivered";
			}
			case 6:{
				return "Cancelled";
			}
		}
	},
	
	validatePhone: function(inputtxt)  
	{
		var pattern1 = /^\(?([0-9]{3})\)?[-]?([0-9]{3})[-]?([0-9]{4})$/;
		var pattern2 = /^\d{10}$/;  
		if(inputtxt.match(pattern1) || inputtxt.match(pattern2))  
		{
			return true;
		}
		else
		{
			return false;
		}
	},	
	
	logOut: function(){
		$.removeCookie('user');
		this.undelegateEvents();
		delete this;
		window.location.reload(true);
	},
	
	switchView: function(){
		$.cookie('user', { name: $.cookie('user').name, isAdmin: true, isDriver: true, view: 1}, {expires: 7});	
		new app.DriverView;
		this.undelegateEvents();
		delete this;
	},

	//Launches the user info jquery dialogue
	//This allows you to change password/email etc
	//This will only be available until Shibboleth
	//integration is done
	showUserInfo: function(e){
		e.preventDefault();
	
		var cloneDialog = this.$('#editUserInfo').clone();
		var self = this;
		cloneDialog.dialog({
			width: "auto",
		
			open: function (event, ui){
				$(this).find('.tabs')
					.attr('id', 'tabs')
					.tabs()
					.addClass('ui-tabs-vertical ui-helper-clearfix');
	
				$(this).find('.exitUserInfo').attr('id','exitUserInfo');

				var tab1 = $(this).find('#fragment-1');	
		
				tab1.find('.changePasswordForm').attr('id', 'changePasswordForm');				
				tab1.find('.newPassword').attr('id', 'newPassword');			
				tab1.find('.currentPassword').attr('id', 'currentPassword');
				tab1.find('.confirmNewPassword').attr('id', 'confirmNewPassword');	
				tab1.find('.changePassError').attr('id', 'changePassError');

				$('#exitUserInfo').click(function(){
					cloneDialog.dialog("close");
				});	
		
				tab1.find('#changePasswordForm').submit(function(e){
					e.preventDefault();
					//Handles change password request
					if($('#newPassword').val() != $('#confirmNewPassword').val()){
						$('#changePassError').find('p').text('New passwords do not match!');
						$('#changePassError').css('display','block');
					} else{
						$.ajax({
							type: "POST",
							url: "/api/users/password",
							data: {	
								password: $('#currentPassword').val(),
								newPassword: $('#newPassword').val()
							},
							success: function(data){
								if(data.status == 401){
									$('#changePassError').find('p').text('Incorrect current password!');	
									$('#changePassError').css('display','block');							
								}
								else if(data.status == 200){
									var notifier = window.notifier = new Backbone.Notifier({
										ms: 2000,
										el: 'body',
										theme: 'plastic',
										type: 'success',
										position: 'top'
									});
									notifier.notify("Password changed!");
									cloneDialog.dialog("close");
								}
							}
						});
					}
				});
		
				tab1.find('#newPassword, #confirmNewPassword, #currentPassword').click(function(){
					$('#changePassError').css('display','none');
				});
				
				var tab2 = $(this).find('#fragment-2');
				
				tab2.find('.changeEmailForm').attr('id', 'changeEmailForm');	
				tab2.find('.userEmail').attr('id', 'userEmail');
				tab2.find('.emailUpdateSubmit').attr('id', 'emailUpdateSubmit');
									
				$.ajax({
					type: "GET",
					url: "/api/users/password",
					success: function(data){
						if(data.status == 200){
							$('#userEmail').val(data.email);
						}
						$('#userEmail').on('input', function(e){
							e.preventDefault();
							$('#emailUpdateSubmit').css('display', 'block');
						});
						
						$('#userEmail').keypress(function(e) {
							if (e.keyCode == '13') {
								//prevents autosubmitting if you press enter in the input box
								e.preventDefault();
								$('#emailUpdateSubmit').css('display', 'block');
							} else{
								$('#emailUpdateSubmit').css('display', 'block');
							}
						});
						
					}
				});
				
				tab2.find('#emailUpdateSubmit').click(function(e){
					e.preventDefault();
					$.ajax({
						type: "PUT",
						url: "/api/admin/users/" + $.cookie('user').name,
						data: {	
							email: $('#userEmail').val()
						},
						success: function(data){
							var notifier = window.notifier = new Backbone.Notifier({
								ms: 2000,
								el: 'body',
								theme: 'plastic',
								type: 'success',
								position: 'top'
							});
															
							if(data.message != null){
								notifier.notify("Email not changed because already the same.");
								cloneDialog.dialog("close");
							} else{
								if(data.status == 200){
									notifier.notify("Email changed!.");
									cloneDialog.dialog("close");										
								} else{
									var notifier = window.notifier = new Backbone.Notifier({
										ms: 2000,
										el: 'body',
										theme: 'plastic',
										type: 'error',
										position: 'top'
									});										
									notifier.notify("Yikes!! Internal server error! Please try again.")
									cloneDialog.dialog("close");
								}
							}
						}
					});							
				});											
			},
		
			close: function (event, ui){
				$(this).dialog("destroy");
				$(this).remove();
			}
		}).prev(".ui-dialog-titlebar").css("background",'#8C8C8C');
	}
});
