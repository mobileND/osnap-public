var app = app || {};

//This page is not up to date. It was determined that it would not be
//used and so development on this page was suspended, but it 
//continued on other pages
app.DriverView = Backbone.View.extend({
	el: '.content',
	
	template: _.template($('#driver-template').html()),
	
	events: {
		'change #requestStatus, #generalStatus' : 'filterByStatus',		
		'click .log-out' : 'logOut',
		'click .switchView' : 'switchView',
		'click span#next' : 'nextPage',
		'click span#prev' : 'prevPage',
		'click .user-info' : 'showUserInfo'		
	},

	initialize: function(options){
		//Change scale to fit iPad better - have to reload so meta tag can go back to original setting
		//the setting is changed when coming in from the login screen, which zooms in. However, if you
		//didn't come from the login page the setting remains the same so therefore no need to refresh
		if(options != null && options.login != null && options.login  == true){
			location.reload();
		}

		//Initialize necessary collections
		//Admin data is needed to generate list 
		//of ride locations as well as max passengers
		//which is used for changing ride info after 
		//request has already been submitted
		this.collection = new app.Rides();
		app.AdminDataList = new app.AdminDataC();
		app.AdminDataList.fetch({reset: true});		
		this.listenTo(this.collection, 'reset', this.render);

		var self = this;
		
		//Socket.io trigger automatic updates when new ride added
		var socket = io.connect(CONFIG.hostname);

		socket.on('ride-update', function (data) {
			self.collection.fetch({
				reset: true
			});
		});			
		
		this.$el.html(this.template);	
		this.collection.fetch({reset: true});					
	},

	//used for pagination
	pageNumber: 1,
	
	maxPages: 0,
	
	//max results per page is 10
	resultsPerPage: 10,
	
	currentRequests: [],
	
	cancelledRequests: [],
	
	render: function(){
		this.filterByStatus();
	},
	
	renderRide: function(ride, alt, cancelled, name){
		var rideView = new app.RideView({
			model: ride,
			assignment: name
		});
		
		if(cancelled){
			this.$('#requests').append(rideView.$el.attr('class', 'requestTableRow cancelled'));			
		}
		else{
			if(alt) this.$('#requests').append(rideView.$el.attr('class', 'requestTableRow alt'))
			else this.$('#requests').append(rideView.$el.attr('class', 'requestTableRow'));			
		}
	},
	
	renderPage: function(name){
		if(this.maxPages > 1 && this.pageNumber != this.maxPages) $('li#next').css('visibility', 'visible');			
		else $('li#next').css('visibility', 'hidden');			
			
		if(this.pageNumber == 1) $('li#prev').css('visibility', 'hidden');
		else $('li#prev').css('visibility', 'visible');						
		
		var count = 0;
		var start = this.pageNumber * this.resultsPerPage - this.resultsPerPage;
		var end = this.pageNumber * this.resultsPerPage; 
		for( var i = start; (i < end) && (i < this.currentRequests.length); i++){
			if(count % 2 == 0){
				if(this.currentRequests[i].attributes.status == 6 && this.currentRequests[i].attributes.cancelled != true){
					this.renderRide(this.currentRequests[i], false, true, name);					
				}
				else this.renderRide(this.currentRequests[i], false, false, name);				
			}
			else{
				if(this.currentRequests[i].attributes.status == 6 && this.currentRequests[i].attributes.cancelled != true){
					this.renderRide(this.currentRequests[i], true, true, name);					
				}
				else this.renderRide(this.currentRequests[i], true, false, name);					
			}
			count++;			
		}
	},	

	filterByStatus: function(){
		this.currentRequests = [];		
		$('.requestTableRow').remove();
		var name = $.cookie('user').name;		
		this.collection.each(function(ride){
			if(ride.attributes.assignedTo == name)
			{
				if(ride.attributes.status == this.$('#requestStatus').val()){
					this.currentRequests.push(ride);
				}			
				else if(ride.attributes.status == "6" && ride.attributes.cancelled != true){
					this.cancelledRequests.push(ride);
				}	
			}
		}, this);
		
		this.pageNumber = 1;
		this.maxPages = Math.ceil(this.currentRequests.length / this.resultsPerPage);
		
		//There are two lists because we want cancelled rides to appear at the top
		//Cancelled rights sit there until they are confirmed by the admin
		this.cancelledRequests.sort(this.sortFunction);
		this.currentRequests.sort(this.sortFunction);
		this.currentRequests = this.cancelledRequests.concat(this.currentRequests);
		this.cancelledRequests = [];
		
		this.renderPage(name);		
	},	
	
	//Sorts by date and time -- most recent at top
	sortFunction: function (a,b) {
		var date1 = new Date(a.attributes.date + ' ' + a.attributes.time);
		var date2 = new Date(b.attributes.date + ' ' + b.attributes.time);
		if( date1 > date2){
			return -1;
		} else if ( date1 < date2 ){
			return 1;
		} else {
			return 0;
		}
	},			
	
	nextPage: function(){
		var name = $.cookie('user').name;		
		if (this.pageNumber < this.maxPages){
			this.pageNumber++;
			$('.requestTableRow').remove();			
			this.renderPage(name);
		}
	},

	prevPage: function(){
		var name = $.cookie('user').name;		
		this.pageNumber--;
		if(this.pageNumber == 0) this.pageNumber = 1;
		$('.requestTableRow').remove();			
		this.renderPage(name);		
	},		
	
	logOut: function(){
		$.removeCookie('user');
		this.undelegateEvents();
		delete this;
		window.location.reload(true);
	},
	
	switchView: function(){
		$.cookie('user', { name: $.cookie('user').name, isAdmin: true, isDriver: true, view: 0}, {expires: 7});	
		new app.RidesView;
		this.undelegateEvents();
		delete this;
	},
	
	//Handles changes to user info like password etc
	//This will be eliminated after Shibboleth integration
	showUserInfo: function(e){
		//You'll notice I find classes and add IDs to them
		//This is because the dialogue windows I'm making are
		//copies so I need to assign an ID so I can reference
		//the elements in the copy. I'm forced to make copies
		//because if I don't--using the JQuery clone dialogue--then
		//it will only open once and can't be opened again
		e.preventDefault();
	
		var cloneDialog = this.$('#editDriverInfo').clone();
		var self = this;
		cloneDialog.dialog({
			width: "auto",
		
			open: function (event, ui){
				$(this).find('.tabs')
					.attr('id', 'tabs')
					.tabs()
					.addClass('ui-tabs-vertical ui-helper-clearfix');
	
					var tab1 = $(this).find('#fragment-1');	
			
					tab1.find('.changePasswordForm').attr('id', 'changePasswordForm');				
					tab1.find('.newPassword').attr('id', 'newPassword');			
					tab1.find('.currentPassword').attr('id', 'currentPassword');
					tab1.find('.confirmNewPassword').attr('id', 'confirmNewPassword');	
					tab1.find('.changePassError').attr('id', 'changePassError');
					tab1.find('.driverPasswordSubmit').attr('id', 'driverPasswordSubmit');						
			
					$('#driverPasswordSubmit').click(function(e){
						e.preventDefault();
						if($('#newPassword').val() != $('#confirmNewPassword').val()){
							$('#changePassError').find('p').text('New passwords do not match!');
							$('#changePassError').css('display','block');
						} else{
							//update password
							$.ajax({
								type: "POST",
								url: "/api/users/password",
								data: {	
									password: $('#currentPassword').val(),
									newPassword: $('#newPassword').val()
								},
								success: function(data){
									if(data.status == 401){
										$('#changePassError').find('p').text('Incorrect current password!');	
										$('#changePassError').css('display','block');							
									}
									else if(data.status == 200){
										var notifier = window.notifier = new Backbone.Notifier({
											ms: 2000,
											el: 'body',
											theme: 'plastic',
											type: 'success',
											position: 'top'
										});
										notifier.notify("Password changed!");
										cloneDialog.dialog("close");
									}
								}
							});							
						}
					});
			
					tab1.find('#newPassword, #confirmNewPassword, #currentPassword').click(function(){
						$('#changePassError').css('display','none');
					});
					
					var tab2 = $(this).find('#fragment-2');
					
					tab2.find('.changeEmailForm').attr('id', 'changeEmailForm');							
					tab2.find('.userEmail').attr('id', 'userEmail');
					tab2.find('.emailUpdateSubmit').attr('id', 'emailUpdateSubmit');
							
					//get current email and set fill in value in box					
					$.ajax({
						type: "GET",
						url: "/api/users/password",
						success: function(data){
							if(data.status == 200){
								$('#userEmail').val(data.email);
							}
							$('#userEmail').on('input', function(e){
								e.preventDefault();
								$('#emailUpdateSubmit').css('display', 'block');
							});
							
							$('#userEmail').keypress(function(e) {
								if (e.keyCode == '13') {
									//prevents autosubmitting if you press enter in the input box
									e.preventDefault();
									$('#emailUpdateSubmit').css('display', 'block');								 
								} else{
									$('#emailUpdateSubmit').css('display', 'block');				               		
								}
							});											
						}
					});
					
					tab2.find('#emailUpdateSubmit').click(function(e){
						e.preventDefault();
						$.ajax({
							type: "PUT",
							url: "/api/admin/users/" + $.cookie('user').name,
							data: {	
								email: $('#userEmail').val()
							},
							success: function(data){
								var notifier = window.notifier = new Backbone.Notifier({
									ms: 2000,
									el: 'body',
									theme: 'plastic',
									type: 'success',
									position: 'top'
								});
																
								if(data.message != null){
									notifier.notify("Email not changed because already the same.");
									cloneDialog.dialog("close");
								} else{
									if(data.status == 200){
										notifier.notify("Email changed!.");
										cloneDialog.dialog("close");										
									} else{
										var notifier = window.notifier = new Backbone.Notifier({
											ms: 2000,
											el: 'body',
											theme: 'plastic',
											type: 'error',
											position: 'top'
										});										
										notifier.notify("Yikes!! Internal server error! Please try again.")
										cloneDialog.dialog("close");
									}
								}
							}
						});							
					});											
			},
		
			close: function (event, ui){
				$(this).dialog("destroy");
				$(this).remove();
			}
		}).prev(".ui-dialog-titlebar").css("background",'#8C8C8C');		
	}			
});
