var app = app || {};

//Used for admins to change permissions of other users
app.ManageAccessView = Backbone.View.extend({

	initialize: function(){	
		
		var self = this;
		var el = this.$el;
		
		$('#users').change(function(){
			self.displayInfo();
		});

		$('#isAdmin').change(function(){
			self.displayOptions();
		});	
		
		$('#isDriver').change(function(){
			self.displayOptions();
		});	
		
		$('form.manageUsers').submit(function(e){
			e.preventDefault();
			self.updateInfo();
		});		
		
		$('#cancel').click(function(e){
			e.preventDefault();
			self.displayInfo();
		});
		
		this.render();
		
	},
	
	render: function(){
		this.collection = app.UserList;
		this.populateFields();												
	},
	
	populateFields: function(){
		$("#userAttributes").css("display", "none");			
		$('#users option').remove();
			
		$('#users').append($('<option></option>')
			.attr('value', "none")
			.text("Please Select"));			
			
		this.collection.each(function(user){
			$('#users').append($('<option></option>')
				.attr('value', user.attributes.username)
				.text(user.attributes.username));								
		}, this);
	},
	
	displayInfo: function(){
		$(".accessOptions").css("display", "none");		
		if($('#users').val() == 'none'){
			$('#userAttributes').css('display', 'none');
		}
		else{
			$('#userAttributes').css('display', 'block');
			$('#success').css('display', 'none');						
			var selectedUser = $('#users').val();
			
			this.collection.each(function(user){
				if( user.attributes.username == selectedUser){
					if(user.attributes.isAdmin == true){
						$('#isAdmin').val("true");						
					} else $('#isAdmin').val("false");

					if(user.attributes.isDriver == true){
						$('#isDriver').val("true");
					} else $('#isDriver').val("false");
					return false; //break loop
				}
			}, this);			
		}
	},
	
	displayOptions: function(){
		$(".accessOptions").css("display", "inline-block");
	},
	
	hideOptions: function(){
		$(".accessOptions").css("display", "none");		
	},
	
	updateInfo: function(){
		var isAdmin = $('#isAdmin').val();
		var isDriver = $('#isDriver').val();
		var username = $('#users').val();		
		var self = this;
		
		$.ajax({
			type: "PUT",
			url: "/api/admin/users/" + username,
			data: {isAdmin: isAdmin, isDriver: isDriver},
			statusCode: {
				500: function() {
					var notifier = window.notifier = new Backbone.Notifier({
						ms: 2000,
						el: '#tabs',
						theme: 'plastic',
						type: 'error',
						position: 'bottom'
					});
					notifier.notify("Failed to update user. Please try again.");
				},
				200: function() {
					var notifier = window.notifier = new Backbone.Notifier({
						ms: 2000,
						el: '#tabs',
						theme: 'plastic',
						type: 'success',
						position: 'bottom'
					});
					notifier.notify("Successfully updated user.");	
					self.hideOptions();
					self.collection.fetch({ reset: true	})				
				}
			}			
		});		
	}								
});