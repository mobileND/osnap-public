var app = app || {};

//Login view only in place until SHhibboleth integration
app.LoginView = Backbone.View.extend({
	el: '.content',
	
	template: _.template($('#login-template').html()),
	
	events: {
		"submit form.login-form" : "login",
		"click a.registeredInfo" : "changeViews",
		"click a.linkForgotInfo" : "forgotInfo"
	},

	initialize: function(){
		var ms_ie = false;
		var ua = window.navigator.userAgent;
		var old_ie = ua.indexOf('MSIE ');
		var new_ie = ua.indexOf('Trident/');

		if ((old_ie > -1) || (new_ie > -1)) {
			window.location.replace("/ie.html");
		} else {
			//Change scale to fit iPad better
			if(navigator.userAgent.match(/iPad/i)){
				$('#viewport').attr("content","user-scalable=yes, initial-scale=2.0");
			}
			this.render();
		}
	},

	render: function(){
		this.$el.html(this.template);
	},
	
	changeViews: function(){
		if($('input[type="submit"]').val() == "Login"){
			$('input[type="submit"]').val("SignUp");
			$('#email').val("");
			$('#email').css('display', 'block');
			$('a.registeredInfo').text("Already registered?");		
			$('p.pForgotInfo').css('display', 'none');
		}
		else{
			$('input[type="submit"]').val("Login");
			$('#email').val("login@login.com"); //need a value to submit form -- this denotes a login attempt & not a registration attempt
			$('#email').css('display', 'none');			
			$('a.registeredInfo').text("Not Registered?");	
			$('p.pForgotInfo').css('display', 'block');
		}	
	},
	
	login: function(e){
		e.preventDefault();
		var username = $('#username').val();
		var password = $('#password').val();
		var self = this;
		
		if($('input[type="submit"]').val() == "Login"){
			$.ajax({
				type: "POST",
				url: "/api/users/authenticate",
				data: {username: username.toLowerCase(), password: password},
				success: function(data){
					if(data.status != null){
						//If status is not null then that means request went through
						//but produced some errors
						if(data.status == 404 || data.status == 401){
							var notifier = window.notifier = new Backbone.Notifier({
								ms: 2500,
								el: 'fieldset',
								theme: 'plastic',
								type: 'error',
								position: 'bottom'
							});
							notifier.notify("Invalid username or password. Please try again.");
						}
						else if(data.status == 500){
							var notifier = window.notifier = new Backbone.Notifier({
								ms: 2500,
								el: 'fieldset',
								theme: 'plastic',
								type: 'error',
								position: 'bottom'
							});
							notifier.notify("Yikes!! Internal Server Error. Please try again.");							
						}
					}
					else
					{
						$('#username').val("");
						$('#password').val("");
						//Sections below are commented out because it has
						//Been decided that the Driver View will not be used
						//at this time						
						/*if(data.isAdmin == true && data.isDriver == true){
							new app.ChooseRoleView({data: data});
							self.undelegateEvents();
							delete self;								
						}*/			
						if(data.isAdmin == true){
							$.cookie('user', { name: data.username, isAdmin: true, isDriver: false, view: 0}, {expires: 7});						
							if(navigator.userAgent.match(/iPad/i)){
								new app.RidesView({login:true});
							} else {
								new app.RidesView();
							}
							self.undelegateEvents();
							delete self;
						}
						/*else if(data.isDriver == true){
							$.cookie('user', { name: data.username, isAdmin: false, isDriver: true, view: 1}, {expires: 7});						
							if(navigator.userAgent.match(/iPad/i)){
								new app.DriverView({login:true});
							} else {
								new app.DriverView();
							}
							self.undelegateEvents();
							delete self;
						}*/
						else{
							window.location.replace("/denied.html");
						}
					}
				}
			});				
		}
		else{
			var username = $('#username').val();
			var regEx = /^[a-zA-Z0-9_-]{3,15}$/;
			if(regEx.test(username)){
				var email = $('#email').val();
				$.ajax({
					type: "POST",
					url: "/api/users",
					data: {username: username.toLowerCase(), password: password, email: email},
					success: function(data){
						if(data.status != null){
							var notifier = window.notifier = new Backbone.Notifier({
								ms: 2500,
								el: 'fieldset',
								theme: 'plastic',
								type: 'error',
								position: 'bottom'
							});
							notifier.notify(data.message);						
						}
						else
						{
							$('#username').val("");
							$('#password').val("");					
							$('.login-form .error').html("").css("display", "none");	
						
							//Information below is commented out because Driver View is currently not in use
							/*$.cookie('user', { name: data.username, isAdmin: false, isDriver: true, view: 1}, {expires: 7});		
							if(navigator.userAgent.match(/iPad/i)){
								new app.DriverView({login:true});
							} else {
								new app.DriverView();
							}
							self.undelegateEvents();
							delete self;*/
							window.location.replace("/registered.html");						
						}
					}
				});	
			} else {
				var notifier = window.notifier = new Backbone.Notifier({
					ms: 7500,
					el: 'fieldset',
					theme: 'plastic',
					type: 'error',
					position: 'bottom'
				});
				notifier.notify("Invalid username!\nPlease use only capital and lowercase letters A-Z, numbers, and underscores or hyphens. The username must be between 3-15 characters long.");
			}			
		}		
	},
	
	forgotInfo: function(){
		var cloneDialog = this.$('#forgotInfo').clone();
		var self = this;
		cloneDialog.dialog({
			width: "auto",
			
			open: function (event, ui){									
				$(this).find('.forgotInfoForm').attr('id', 'forgotInfoForm');		
				$(this).find('.forgotInfoEmail').attr('id', 'forgotInfoEmail');
				$(this).find('.forgotInfoSubmit').attr('id', 'forgotInfoSubmit');															
				$(this).find('.forgotInfoExit').attr('id', 'forgotInfoExit');

				$('#forgotInfoSubmit').click(function(e){
					e.preventDefault();
					$.ajax({
						type: "POST",
						url: "/api/users/reset",
						data: {email: $('#forgotInfoEmail').val()},
						success: function(data){
							if(data.status == 401){
								var notifier = window.notifier = new Backbone.Notifier({
									ms: 2500,
									el: 'div#forgotInfo',
									theme: 'plastic',
									type: 'error',
									position: 'bottom'
								});
								notifier.notify("The email you entered is not associated with an account!");						
							} else if (data.status == 500){
								var notifier = window.notifier = new Backbone.Notifier({
									ms: 2500,
									el: 'fieldset',
									theme: 'plastic',
									type: 'error',
									position: 'bottom'
								});
								notifier.notify(data.message);	
								cloneDialog.dialog("close");
							} else {
								var notifier = window.notifier = new Backbone.Notifier({
									ms: 2500,
									el: 'fieldset',
									theme: 'plastic',
									type: 'success',
									position: 'bottom'
								});
								notifier.notify("Account information has been emailed to you!");									
								cloneDialog.dialog("close");
							}
						}
					});						
				});

				$('#forgotInfoExit').click(function(e){
					e.preventDefault();
					cloneDialog.dialog("close");
				});

			},
			
			close: function (event, ui){
				$(this).dialog("destroy");
				$(this).remove();
			}
		}).prev(".ui-dialog-titlebar").css("background",'#8C8C8C');				
	}	
});
