var app = app || {};

app.RideView = Backbone.View.extend({
	tagName: 'tr',
	className: 'requestTableRow',
	template: _.template($('#request-template').html()),
	
	events: {
		'change #closed' : 'updateClosed',
		'change #status' : 'updateStatus',
		'change #assignment' : 'updateAssignment',
		'change #location' : 'updateLocation',
		'change #destination' : 'updateDestination',
		'change #numberPassengers' : 'updateNumberPassengers',		
		'click #viewLog' : 'showIndvLog',
		'click #confirmCancel' : 'confirmCancel',		
		'click #deleteRide' : 'deleteRide'
	},
	
	assignment: null,

	id: null,
	
	initialize: function(options){
		this.assignment = options.assignment;
		this.listenTo(this.model, 'save', this.render);
		this.render();
		this.listenTo(this.model, 'change', this.render);		
	},
	
	render: function(){
		//This.assignment is used to specify that the driver view is 
		//instantiating this Ride. When this happens different options are needed
		// model.attributes.showLog is used to control options in the template
		
		if(this.assignment != null) this.model.attributes.showLog = false;
		this.$el.html(this.template(this.model.attributes));
		this.id = this.model.attributes._id;
		
		if(this.model.attributes.closed == true){
			
			this.$('#closed').val('true');
			if(this.assignment != null){
				this.$('#closed option[value="false"]').remove();
				this.$('#closed').prop('disabled', 'disabled');
			}
		}
		else{
			this.$('#closed').val('false');	
			if(this.assignment != null){
				this.$('#closed option[value="true"]').remove();
				this.$('#closed').prop('disabled', 'disabled');
			}					
		} 
					
		this.$('#status').val(this.model.attributes.status);
		var self = this;
		
		if(this.assignment != null){		
			//Handle Driver Selection Dropdown
			self.$('#assignment option')
				.remove();
			
			self.$('#assignment')
				.append($('<option></option>')
				.attr("value", this.assignment)
				.text(this.assignment)).prop('disabled', 'disabled'); 
				//disable changing select because driver does not have power to do so
		}
		else{
			//Handle Driver Selection Dropdown
			app.UserList.each( function(user){
				if(user.attributes.isDriver == true){
					self.$('#assignment')
						.append($('<option></option>')
						.attr("value", user.attributes.username)
						.text(user.attributes.username));					
				}	
			}, app.UserList);	
			
			//Disable assignment dropdown. This is done so you can only
			//Assign a driver after the request has been confirmed by 
			//the admin
			
			if(this.$('#status').val() == 0 || this.$('#status').val() == 5 || this.$('#status').val() == 6 ){
				this.$('#assignment').prop('disabled', 'disabled');
			}					
		}
		
		//Handle Location & Destination Selection Dropdown -- So that changes can be made
		_.each(app.AdminDataList.models[0].attributes.locations,function(location){
			self.$('#location')
				.append($('<option></option>')
				.attr("value", location.name)
				.text(location.name));		
			
			self.$('#destination')
				.append($('<option></option>')
				.attr("value", location.name)
				.text(location.name));						
		});
		
		self.$('#location').val(this.model.attributes.location);
		self.$('#destination').val(this.model.attributes.destination);
		/////////////		
		
		//Handle Number of Passengers -- So that changes can be made
		for(var i = 0; i < app.AdminDataList.models[0].attributes.maxRiders; i++ ){
			self.$('#numberPassengers')
				.append($('<option></option>')
				.attr("value", i + 1)
				.text(i + 1));
		}
		
		self.$('#numberPassengers').val(this.model.attributes.numberPassengers);
		/////////////
		
		if(this.model.attributes.assignedTo != null){
			this.$('#assignment').val(this.model.attributes.assignedTo);
		}
		
		//If cancelled
		if(this.model.attributes.status == 6 && this.model.attributes.cancelled == false ){
			this.$('#confirmCancel').css('display', 'inline-block');
		}
		
		//Fill in log info to the template
		var message = "";
		for (var i = 0; i < this.model.attributes.log.length; i++){
			message = this.parseLogEntry(this.model.attributes.log[i]);
			this.$('#logList')
				.append($('<li></li>')
				.text(message));				 
		}		
												
	},
	
	updateClosed: function(){
		var newVal = (this.$('#closed').val() == "true");
		this.model.set({'closed': newVal});
		this.model.save(null);
	},
	
	updateStatus: function(){
		if(this.$('#status').val() == 0){
			this.$('#assignment').prop('disabled', 'disabled');
		}		
		else this.$('#assignment').prop('disabled', false);
		
		var newVal = this.$('#status option:selected').val();
		if(newVal != 6){
			//This was done because rides that had previously been marked cancelled
			//kept their cancelled attributed being true even after their status
			//attribute was changed to something else
			this.model.set({'cancelled': false});
		}
		this.model.set({'status': newVal});
		this.model.save(null);
	},
	
	//Update driver on ride
	updateAssignment: function(){
		var newVal = this.$('#assignment option:selected').val();
		if(newVal == 'n/a') newVal = "";
		this.model.set({'assignedTo': newVal});
		this.model.save(null, {
			success: function(model, response){
				var notifier = window.notifier = new Backbone.Notifier({
					ms: 1500,
					theme: 'plastic',
					type: 'success'
				});
				notifier.notify("Successfully assigned driver.");	
			},
			error: function(model, response){
				var notifier = window.notifier = new Backbone.Notifier({
					ms: 1500,
					theme: 'plastic',
					type: 'error'
				});
				notifier.notify("Error assigning driver. Try again.");	
			},				
		});
	},
	
	updateLocation: function(){
		var newVal = this.$('#location option:selected').val();
		this.model.set({'location': newVal});
		this.model.save(null, {
			success: function(model, response){
				var notifier = window.notifier = new Backbone.Notifier({
					ms: 1500,
					theme: 'plastic',
					type: 'success'
				});
					notifier.notify("Successfully updated location.");	
			},
			error: function(model, response){
				var notifier = window.notifier = new Backbone.Notifier({
					ms: 1500,
					theme: 'plastic',
					type: 'error'
				});
				notifier.notify("Error updating location. Try again.");	
			},				
		});
	},
	
	updateDestination: function(){
		var newVal = this.$('#destination option:selected').val();
		this.model.set({'destination': newVal});
		this.model.save(null, {
			success: function(model, response){
				var notifier = window.notifier = new Backbone.Notifier({
					ms: 1500,
					theme: 'plastic',
					type: 'success'
				});
					notifier.notify("Successfully updated destination.");	
			},
			error: function(model, response){
				var notifier = window.notifier = new Backbone.Notifier({
					ms: 1500,
					theme: 'plastic',
					type: 'error'
				});
				notifier.notify("Error updating destination. Try again.");	
			},				
		});	
	},	
	
	updateNumberPassengers: function(){
		var newVal = this.$('#numberPassengers option:selected').val();
		this.model.set({'numberPassengers': newVal});
		this.model.save(null, {
			success: function(model, response){
				var notifier = window.notifier = new Backbone.Notifier({
					ms: 1500,
					theme: 'plastic',
					type: 'success'
				});
				notifier.notify("Successfully updated passengers");	
			},
			error: function(model, response){
				var notifier = window.notifier = new Backbone.Notifier({
					ms: 1500,
					theme: 'plastic',
					type: 'error'
				});
				notifier.notify("Error updating passengers. Try again.");	
			},				
		});			
	},
		
	showIndvLog: function(){		
		var cloneDialog = this.$('#log').clone();
		var self = this;
		cloneDialog.dialog({
			width: "auto",
			
			open: function (event, ui){
				$(this).find('.exitLog').attr('id','exitLog');
				$('#exitLog').click(function(e){
					cloneDialog.dialog("close");
				});
			},
			
			close: function (event, ui){
				$(this).dialog("destroy");
				$(this).remove();
			}
		}).prev(".ui-dialog-titlebar").css("background",'#8C8C8C');		
	},
	
	//parses logs that are returned with the object from the DB
	parseLogEntry: function(message){
		var values = message.split(';');
		if(values.length == 1){
			return values[0];
		}
		else{
			switch(values[0]){
				case "status":{
					return 'Status Change: "' + this.numberToStatus(parseInt(values[1])) + '" to "' + this.numberToStatus(parseInt(values[2])) + '" -- ' + values[3];
				}
				case "closed":{
					if(values[1] == "false"){
						return 'Request changed from "open" to "closed" -- ' + values[3];					
					}	
					else{
						return 'Request changed from "closed" to "open" -- ' + values[3];						
					}		
				} 
				case "driver":{
					if(values[1] == null || values[1] == ""){
						return 'Driver "' + values[2] + '" has been assigned to this ride -- ' + values[3];					
					}
					else{
						return 'Driver change: "' + values[1] + '" to "' + values[2] + '" -- ' + values[3];					
					}
				}
				case "cancelled":{
					return values[1].toString();
				}	
				case "location":{
					return 'Location Change: "' + values[1] + '" to "' + values[2] + '" -- ' + values[3];
				}	
				case "destination":{
					return 'Destination Change: "' + values[1] + '" to "' + values[2] + '" -- ' + values[3];
				}					
				case "passengers":{
					return 'Passengers changed from "' + values[1] + '" to "' + values[2] + '" -- ' + values[3]; 
				}				
			}			
		}
	},

	deleteRide: function() {	
		this.model.destroy({
			contentType : 'application/json',
			dataType: "text",
			success: function(model, response){
				var notifier = window.notifier = new Backbone.Notifier({
					ms: 2000,
					el: 'body',
					theme: 'plastic',
					type: 'success',
					position: 'top'
				});
				notifier.notify("Successfully deleted ride request!");
			},
			error: function(model, response){
				var notifier = window.notifier = new Backbone.Notifier({
					ms: 2000,
					el: 'body',
					theme: 'plastic',
					type: 'error',
					position: 'top'
				});
				notifier.notify("Error deleting ride request! Please try again!");
			}
		});
	},
	
	//This is called if the admin/driver confirms the cancellation of the ride
	confirmCancel: function(){
		this.model.set({'cancelled': true});
		this.model.save(null);		
	},
	
	//Converts status attribute number to text
	numberToStatus: function(number){
		switch(number){
			case 0:{
				return "New";
			}
			case 1:{
				return "Confirmed by Admin";
			}
			case 2:{
				return "Accepted by Driver";
			}
			case 3:{
				return "Picked up";
			}
			case 4:{
				return "No show";
			}
			case 5:{
				return "Delivered";
			}
			case 6: {
				return "Cancelled";
			}
		}
	}		
});
