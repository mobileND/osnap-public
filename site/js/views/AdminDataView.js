var app = app || {};

app.AdminDataView = Backbone.View.extend({

	initialize: function(){
		
		this.collection = app.AdminDataList;			
		this.listenTo(this.collection, 'reset', this.render);

		//Connect event handlers
		var self = this;
		$('#newLocationForm').submit(function(e){
			e.preventDefault();
			self.addNewLocation();
		});

		$('#updateButton').click(function(e){
			e.preventDefault();			
			self.updateMaxRiders();
		});
		
		$('#disableButton').click(function(e){
			e.preventDefault();			
			self.disableLocation();
		});
				
		$('#enableButton').click(function(e){
			e.preventDefault();			
			self.enableLocation();
		});
						
		$('#deleteButton').click(function(e){
			e.preventDefault();			
			self.deleteLocation(false);
		});	
			
		$('#deleteButtonDisabled').click(function(e){
			e.preventDefault();			
			self.deleteLocation(true);
		});
			
		$('#maxPassengersNumber').click(function(e){
			$('#successPassengers').css('display', 'none');	
		});
			
		$('#serviceRadio1').click(function(){
			self.updateStatus(1);
		});	
		
		$('#serviceRadio2').click(function(){
			self.updateStatus(0);
		});

		this.render();	
	},
	
	render: function(){
		this.populateFields();												
	},
	
	populateFields: function(){
		//Set Max Passengers
		$('#disabledPlaces option').each(function(){
			$(this).remove();
		});
		$('#activePlaces option').each(function(){
			$(this).remove();
		});	
		$('#maxPassengersNumber').val(this.collection.models[0].attributes.maxRiders);
		
		if(this.collection.models[0].attributes.serviceStatus == 1){
			$('#serviceRadio1').attr('checked', 'true');
		} else{
			$('#serviceRadio2').attr('checked', 'true');
		}

		//Set Locations
		var data = this.collection.models[0].attributes.locations;
		for(var i = 0; i < data.length; i++){
			if(data[i].disabled == true){
				$('#disabledPlaces').append($('<option></option>')
					.attr('value', data[i].name)
					.text(data[i].name));					
			}
			else{
				$('#activePlaces').append($('<option></option>')
					.attr('value', data[i].name)
					.text(data[i].name));				
			}
		}

	},
	
	addNewLocation: function(){
		var newLocation = $('#newLocationName').val();
		var newDescription = $('#newLocationDes').val();
		var newShortName = $('#newShortName').val();
		var self = this;
		
		$.ajax({
			type: "POST",
			url: "/api/admin/admindata",
			data: {name: newLocation, disabled: false, description: newDescription, shortName: newShortName},
			statusCode: {
				500: function() {
					var notifier = window.notifier = new Backbone.Notifier({
						ms: 2000,
						el: '#tabs',
						theme: 'plastic',
						type: 'error',
						position: 'bottom'
					});
					notifier.notify("Failed to add new location. Please try again.");
				},
				200: function() {
					var notifier = window.notifier = new Backbone.Notifier({
						ms: 1500,
						el: '#tabs',
						theme: 'plastic',
						type: 'success',
						position: 'bottom'
					});
					notifier.notify("Successfully added new location.");	
					
					self.collection.fetch({
						reset: true,
				
						success: function(collection, response, options){
							$('#newLocationName').val("");	
							$('#newLocationDes').val("");	
							$('#newShortName').val("");					
						}
					});					
				}
			}			
		});
	
	},

	updateMaxRiders: function(){
		var newVal = $('#maxPassengersNumber').val();
		var self = this;		
		
		$.ajax({
			type: "PUT",
			url: "/api/admin/admindata",
			data: {maxRiders: newVal},
			statusCode: {
				500: function() {
					var notifier = window.notifier = new Backbone.Notifier({
						ms: 2000,
						el: '#tabs',
						theme: 'plastic',
						type: 'error',
						position: 'bottom'
					});
					notifier.notify("Failed to update max passengers. Please try again.");
				},
				200: function() {
					var notifier = window.notifier = new Backbone.Notifier({
						ms: 2000,
						el: '#tabs',
						theme: 'plastic',
						type: 'success',
						position: 'bottom'
					});
					notifier.notify("Successfully updated max passengers.");						
					self.collection.fetch({ reset: true	});				
				}
			}
		});
	
	},

	updateStatus: function(status){
		var self = this;

		$.ajax({
			type: "PUT",
			url: "/api/admin/admindata",
			data: {serviceStatus: status},
			statusCode: {
				500: function() {
					var notifier = window.notifier = new Backbone.Notifier({
						ms: 2000,
						el: '#tabs',
						theme: 'plastic',
						type: 'error',
						position: 'bottom'
					});
					notifier.notify("Failed to update service status. Please try again!");
				},
				200: function() {
					var notifier = window.notifier = new Backbone.Notifier({
						ms: 2000,
						el: '#tabs',
						theme: 'plastic',
						type: 'success',
						position: 'bottom'
					});
					notifier.notify("Successfully updated service status!");

					if(status == 1){
						$('#serviceRunning').css('display', 'block');
						$('#serviceStopped').css('display', 'none');
					} else {
						$('#serviceRunning').css('display', 'none');
						$('#serviceStopped').css('display', 'block');								
					}
					self.collection.fetch({ reset: true });						
				}
			}
		});
	},

	disableLocation: function(){
		var location = $('#activePlaces').val();
		var self = this;
		
		$.ajax({
			type: "PUT",
			url: "/api/admin/admindata",
			data: {name: location, disabled: true},
			statusCode: {
				500: function() {
					var notifier = window.notifier = new Backbone.Notifier({
						ms: 2000,
						el: '#tabs',
						theme: 'plastic',
						type: 'error',
						position: 'top'
					});
					notifier.notify("Failed to disable location. Please try again.");
				},
				200: function() {
					var notifier = window.notifier = new Backbone.Notifier({
						ms: 1000,
						el: '#tabs',
						theme: 'plastic',
						type: 'success',
						position: 'top'
					});
					notifier.notify("Location disabled.");					
					self.collection.fetch({ reset: true	});				
				}
			}			
		});
	
	},
	
	enableLocation: function(){
		var location = $('#disabledPlaces').val();
		var self = this;
		
		$.ajax({
			type: "PUT",
			url: "/api/admin/admindata",
			data: {name: location, disabled: false},
			statusCode: {
				500: function() {
					var notifier = window.notifier = new Backbone.Notifier({
						ms: 2000,
						el: '#tabs',
						theme: 'plastic',
						type: 'error',
						position: 'top'
					});
					notifier.notify("Failed to ena=able location. Please try again.");
				},
				200: function() {
					var notifier = window.notifier = new Backbone.Notifier({
						ms: 1000,
						el: '#tabs',
						theme: 'plastic',
						type: 'success',
						position: 'top'
					});
					notifier.notify("Location enabled.");	
					
					self.collection.fetch({ reset: true	});				
				}
			}			
		});	
	},
	
	deleteLocation: function(disabled){
		var location;
		var self = this;
		
		if(disabled) {
			location = $('#disabledPlaces').val();			
		}
		else{
			location = $('#activePlaces').val();
		}
		
		$.ajax({
			type: "DELETE",
			url: "/api/admin/admindata",
			data: {name: location},
			statusCode: {
				500: function() {
					var notifier = window.notifier = new Backbone.Notifier({
						ms: 2000,
						el: '#tabs',
						theme: 'plastic',
						type: 'error',
						position: 'top'
					});
					notifier.notify("Failed to delete location. Please try again.");
				},
				200: function() {
					var notifier = window.notifier = new Backbone.Notifier({
						ms: 1500,
						el: '#tabs',
						theme: 'plastic',
						type: 'success',
						position: 'top'
					});
					notifier.notify("Successfully deleted location.");						
					self.collection.fetch({ reset: true	});				
				}
			}			
		});	
	}										
});
