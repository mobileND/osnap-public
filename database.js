var mongoose = require('mongoose'),
	bcrypt = require('bcrypt'),
	SALT_WORK_FACTOR = 10;
	
// Connect to DB /////////////////////////////
mongoose.connect('mongodb://localhost/osnap_database');

/* Schemas */
var User = new mongoose.Schema({
	username: { type: String, required: true, index: {unique: true}},
	password: { type: String, required: true},
	email: {type: String}, //Required on the client side; not required here cause it makes you have to submit an email address for every update to a User object
	isAdmin: Boolean,
	isDriver: Boolean
});

//This handles encrypting the password
User.pre('save', function(next){
	var user = this;

	//only hash the password if it has been modified (or is new)
	if (!user.isModified('password')) return next();

	//generate a salt
	bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt){
		if(err) return next(err);
	
		//hash the password along with our new salt
		bcrypt.hash(user.password, salt, function(err, hash){
			if(err) return next(err);
		
			//override the text password with the hashed one
			user.password = hash;
			next();
		});
	});
});

//Verify Password
User.methods.comparePassword = function(candidatePassword, cb) {
	bcrypt.compare(candidatePassword, this.password, function(err, isMatch){
		if(err) return cb(err);
		cb(null, isMatch);
	});
};

//When return user object through API do not return password or email
User.methods.toJSON = function(){
	var obj = this.toObject();
	delete obj.password;
	delete obj.email;
	return obj;
};

var RideRequest = new mongoose.Schema({
	location: String,
	destination: String,
	name: String,
	numberPassengers: Number,
	phone: String,
	closed: Boolean,
	cancelled: Boolean,
	status: Number,
	createdAt: String,
	time: String,
	date: String,
	assignedTo: String,
	log: [String]
});

//Add user object 
RideRequest.methods.assignToDriver = function(username, cb){ 
	if(username == ""){
		cb(null, true);
	}
	else
	{
		UserModel.findOne({username: username}, function(err, user){
			if(err) return cb(err);

			if(user != null) cb(null, true);	
			else cb(null, false);			
		});		
	}
};

var Place = new mongoose.Schema({
	name: String,
	disabled: Boolean,
	description: String,
	shortName: String
});

var AdminData = new mongoose.Schema({
	maxRiders: Number,
	serviceStatus: Number,
	locations: [ Place ]
});

var UserModel = mongoose.model('User', User);
var RideRequestModel = mongoose.model('RideRequest', RideRequest);
var PlaceModel= mongoose.model('Place', Place);
var AdminDataModel = mongoose.model('AdminData', AdminData);

exports.UserModel = UserModel;
exports.RideRequestModel = RideRequestModel;
exports.PlaceModel = PlaceModel;
exports.AdminDataModel = AdminDataModel;
